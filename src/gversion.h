#ifndef GVERSION_H
#define GVERSION_H

#define VERSION 0,1,2,0
#define VERSION_STR "0.1.2.0"
#define PROG_NAME "GammaSQLGit"
#define ORG_NAME "Gamma, Orel"

#endif // GVERSION_H
