#ifndef DLGSQLCONNECTION_H
#define DLGSQLCONNECTION_H

#include <QDialog>
#include <QMessageBox>

#include "gversion.h"
#include "gdescriptionconntion.h"
#include "gdb.h"

namespace Ui {
    class DlgSQLConnection;
}

class DlgSQLConnection : public QDialog
{
    Q_OBJECT
    
    public:
        explicit DlgSQLConnection(QWidget *parent = 0);
        ~DlgSQLConnection();

        static gDescriptionConnection getConnection();
        static bool getConnection(gDescriptionConnection &connection);

    private slots:
        void on_pbClose_clicked();
        void on_pbSave_clicked();
        void on_pbTest_clicked();

    private:
        Ui::DlgSQLConnection *ui;
        gDescriptionConnection getValues();
};

#endif // DLGSQLCONNECTION_H
