#include "ggitdiffhighlighter.h"

gGitDiffHighlighter::gGitDiffHighlighter(QTextDocument *parent) :
    QSyntaxHighlighter(parent)
{
}

void gGitDiffHighlighter::highlightBlock(const QString &text)
{
    if (text.mid(0,1)=="-")
        setFormat(0,text.length(),Qt::red);
    else if (text.mid(0,1)=="+")
        setFormat(0,text.length(),Qt::darkGreen);
    else if (text.mid(0,2)=="@@")
        setFormat(0,text.length(),Qt::darkCyan);
}
