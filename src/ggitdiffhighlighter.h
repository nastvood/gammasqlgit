#ifndef GGITDIFFHIGHLIGHTER_H
#define GGITDIFFHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextDocument>
#include <QDebug>

class gGitDiffHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

    public:
        explicit gGitDiffHighlighter(QTextDocument *parent);

    protected:
        void highlightBlock(const QString &text);
    
};

#endif // GGITDIFFHIGHLIGHTER_H
