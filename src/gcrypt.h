#ifndef GCRYPT_H
#define GCRYPT_H

#include <QByteArray>
#include <QCryptographicHash>
#include <QString>
#include <QDebug>

#include <botan/botan.h>

class gCrypt
{
    public:
        gCrypt();
        static QByteArray encode(const QString &password, const QByteArray &msg, bool *ok=NULL);
        static QByteArray decode(const QString &password, const QByteArray &msg, bool *ok=NULL);
        static QByteArray encodePassword(const QString &password,const QByteArray &msg,bool *ok=NULL);
        static QByteArray decodePassword(const QString &password, const QByteArray &msg, bool *ok=NULL);
        static QString getCryptLibraryVersion();

    private:
        static int repeated;
};

#endif // GCRYPT_H
