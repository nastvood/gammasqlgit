#include "gdb.h"

QVector<gObjectDBType> gDB::gObjectDBTypeList=QVector<gObjectDBType>()<<GDB_F<<GDB_P<<GDB_T<<GDB_V;

gDB::gDB(QObject *parent) :
    QObject(parent)
{
    QString connName=gHelper::createDbConnectionName();
    db=QSqlDatabase::addDatabase("QODBC",connName);
    db.setConnectOptions("SQL_ATTR_PACKET_SIZE=500000");

    QFile file(":/scripts/create_table.sql");
    if (file.open(QIODevice::ReadOnly)){
        createTableScript=file.readAll();
        file.close();
    }

    file.setFileName(":/scripts/create_trigger.sql");
    if (file.open(QIODevice::ReadOnly)){
        createTriggersScript=file.readAll();
        file.close();
    }
}

gDB::~gDB()
{
    if (isOpen())
        db.close();
}

void gDB::setDatabaseName(const QString &host, const QString &dbName, const QString &user, const QString &pass)
{
    QString conn=gDB::createConnectionString(host,dbName,user,pass);
    db.setDatabaseName(conn);
}

void gDB::getListOfSQLObjcts(gListSQlObjts &objects,gObjectDBType objType)
{
    objects.clear();

    if (!isOpen())
        if (!db.open())
            return;

    gSQLObj obj;
    QString sql=QString("SELECT o.object_id,o.name,o.create_date,o.modify_date,s.name "
                        " FROM sys.objects o INNER JOIN sys.schemas s ON o.schema_id=s.schema_id "
                        " WHERE type IN (%1) ORDER BY o.name,s.name")
                            .arg(getTypeObjChars(objType));

    QSqlQuery query(sql,db);
    while (query.next()){
        obj.idObj=query.value(0).toULongLong();
        obj.name=query.value(1).toString();
        obj.type=SOT_SP;
        obj.createAt=query.value(2).toDateTime();
        obj.updateAt=query.value(3).toDateTime();
        obj.checked=false;
        obj.schema=query.value(4).toString();

        objects.append(obj);
    }
}

QByteArray gDB::getObjText(const QString &objName_,gObjectDBType objType)
{
    if (!isOpen())
        if (!db.open())
            return QByteArray();

    QString schemaName=objName_.mid(objName_.lastIndexOf(".")+1);
    QString objName=objName_.left(objName_.lastIndexOf("."));

    QSqlQuery query(db);

    if (objType==GDB_T){
        QString sqlTemplate=createTableScript;
        QString sqlScript=sqlTemplate.replace("{table_name}",objName).replace("{schema_name}",schemaName);
        if (!query.exec(sqlScript))
            return QByteArray();
        if(!query.first()){//почему-то с первого раза не возвращает данные
            if (!query.exec(sqlScript))
                return QByteArray();
            if(!query.first())
                return QByteArray();
        }

        QByteArray tableCreateScript=query.value(0).toByteArray();
        tableCreateScript.append(getTriggerOfTable(objName,schemaName));
        return tableCreateScript;
    }

    QString objFullName="["+schemaName+"].["+objName+"]";

    if(!query.exec(QString("SELECT definition FROM sys.sql_modules WHERE object_id=OBJECT_ID('%1')").arg(objFullName)))
        return QByteArray();
    if(!query.next())
        return QByteArray();

    return query.value(0).toByteArray();
}


QByteArray gDB::getTriggerOfTable(const QString &table, const QString &schema)
{
    if (!isOpen())
        if (!db.open())
            return QByteArray();

    QString sqlTemplate=createTriggersScript;
    QString sqlScript=sqlTemplate.replace("{table_name}",table).replace("{schema_name}",schema);

    QSqlQuery query(db);
    if (!query.exec(sqlScript))
        return QByteArray();

    if(!query.first())
        return QByteArray();

    return query.value(0).toByteArray();
}


bool gDB::isOpen()
{
    if (!db.isOpen())
        return false;

    SQLINTEGER dead;
    SQLINTEGER len;
    SQLHANDLE hdbc=*static_cast<void**>(db.driver()->handle().data());
    SQLRETURN ret=SQLGetConnectAttr(hdbc,SQL_ATTR_CONNECTION_DEAD,&dead,sizeof(dead),&len);

    if (ret==SQL_SUCCESS){
        if (dead)
            return false;
        else
            return true;
    }

    return false;
}

QString gDB::createConnectionString(const QString &host, const QString &dbName, const QString &user, const QString &pass)
{
    return QString("Driver={Sql Server};Server=%1;Database=%2;UID=%3;PWD=%4")
                        .arg(host)
                        .arg(dbName)
                        .arg(user)
                        .arg(pass);
}

QString gDB::getTypeObjChars(gObjectDBType objType)
{
    switch (objType){
        case GDB_P:
            return "'P'";
        case GDB_F:
            return "'AF','FN','TF'";
        case GDB_T:
            return "'U'";
        case GDB_V:
            return "'V'";
        default:
            return "P";
    }
}



