#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    globalSettings=new DlgGlobalSettings(ORG_NAME,PROG_NAME,this);

    recentlySettings=new QSettings(ORG_NAME,PROG_NAME,this);
    QHash<QString, QVariant> hash=recentlySettings->value("list").toHash();

    QStringList keyList=gHelper::reverseStringList(hash.keys());
    QStringList removeRecentlyProjects;
    for (int i=keyList.count()-1;i>=0;i--){
        if (!gProject::isValidProject(hash[keyList.at(i)].toString())){
            removeRecentlyProjects<<hash[keyList.at(i)].toString();
            continue;
        }

        QAction *action=new QAction(hash[keyList.at(i)].toString(),this);
        ui->menuRecently->addAction(action);
        connect(action,SIGNAL(triggered()),SLOT(openRecentlyProject()));
    }
    removeRecentlySetting(removeRecentlyProjects);

    area=ui->area;
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    area->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    area->setVisible(true);

    mapper=new QSignalMapper(this);
    connect(mapper,SIGNAL(mapped(QWidget*)),SLOT(setActiveSubWindow(QWidget*)));

    dlgCreateProject=new DlgCreateProject(this);

    actCreateProject=new QAction(QIcon(":/img/new.png"),"Создать проект",this);
    ui->mainToolBar->addAction(actCreateProject);
    connect(actCreateProject,SIGNAL(triggered()),SLOT(showDlgCreateProject()));
    actOpenProject=new QAction(QIcon(":/img/open.png"),"Открыть проект",this);
    ui->mainToolBar->addAction(actOpenProject);
    connect(actOpenProject,SIGNAL(triggered()),SLOT(showDlgOpenProject()));
    ui->mainToolBar->addSeparator();
    actGlSettings=new QAction(QIcon(":/img/glsettings.png"),"Настройки",this);
    ui->mainToolBar->addAction(actGlSettings);
    connect(actGlSettings,SIGNAL(triggered()),SLOT(showDlgGlobalSettings()));
    ui->mainToolBar->addSeparator();
    actClose=new QAction(QIcon(":/img/quit.png"),"Выход",this);
    ui->mainToolBar->addAction(actClose);
    connect(actClose,SIGNAL(triggered()),SLOT(close()));

    actAbout=new QAction("О программе",this);
    connect(actAbout,SIGNAL(triggered()),SLOT(showAbout()));
    ui->menuBar->addAction(actAbout);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setActiveSubWindow(QWidget *window)
{
    if (!window)
        return;

    area->setActiveSubWindow(qobject_cast<QMdiSubWindow *>(window));
}

void MainWindow::openProject(const QString &projectName, const QString &projectPath,const QString &projectPassword, bool create)
{
    if (isOpenedProject(projectName,projectPath)){
        QMessageBox::information(this,PROG_NAME,"Данный проект уже открыт!",QMessageBox::Ok);
        return;
    }

    gProject *project=new gProject(projectName,projectPath,globalSettings,this);
    if (project->openProject(projectPassword,create)){
        connect(project,SIGNAL(sigMessActionText(QString)),ui->statusBar,SLOT(showMessage(QString)));
        area->addSubWindow(project);
        project->show();
    }else{
        project->release();
    }
}

void MainWindow::removeRecentlySetting(QStringList &paths)
{
    QHash<QString, QVariant> hash=recentlySettings->value("list").toHash();

    if ((!hash.count()) || (!paths.count()))
        return;

    for (int i=0;i<paths.count();i++){
        hash.remove(paths.at(i));
    }

    recentlySettings->setValue("list",hash);
}

void MainWindow::showDlgCreateProject()
{
    QString projectName;
    QString projectPath;
    QString projectPassword;
    bool ok;

    dlgCreateProject->getCreateProjectAttrs(projectName,projectPath,projectPassword,ok);
    if (ok){
        openProject(projectName,projectPath,projectPassword);
        appendRecentlyAction(projectPath+QDir::separator()+projectName);
    }
}

void MainWindow::showDlgOpenProject()
{
    QString projectPath=QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this,"Папка"));
    if (!projectPath.isEmpty()){
        QString projectName=projectPath.mid(projectPath.lastIndexOf(QDir::separator())+1);
        if (!projectName.isEmpty()){
            QString pathToProjectDir=projectPath.mid(0,projectPath.lastIndexOf(projectName));

            bool ok;
            QIcon icon=this->windowIcon();
            this->setWindowIcon(QIcon(":/img/password.png"));
            QString projectPassword=QInputDialog::getText(this,
                                               "Пароль",
                                               "Ведите пароль для проекта",
                                               QLineEdit::Password,QString(),&ok);
            this->setWindowIcon(icon);
            if (!ok)
                return;

            openProject(projectName,pathToProjectDir,projectPassword,false);
            appendRecentlyAction(projectPath);
        }
    }
}

void MainWindow::openRecentlyProject()
{
    QString projectFullPath=qobject_cast<QAction *>(sender())->text();
    if (!projectFullPath.isEmpty()){
        QString projectName=projectFullPath.mid(projectFullPath.lastIndexOf("\\")+1);
        if (!projectName.isEmpty()){
            QString projectPath=projectFullPath.mid(0,projectFullPath.lastIndexOf(projectName));

            bool ok;
            QIcon icon=this->windowIcon();
            this->setWindowIcon(QIcon(":/img/password.png"));
            QString projectPassword=QInputDialog::getText(this,
                                               "Пароль",
                                               "Ведите пароль для проекта",
                                               QLineEdit::Password,QString(),&ok);
            this->setWindowIcon(icon);
            if (!ok)
                return;

            openProject(projectName,projectPath,projectPassword,false);
            appendRecentlyAction(projectFullPath);
        }
    }
}


void MainWindow::showDlgGlobalSettings()
{
    globalSettings->show();
}

void MainWindow::showAbout()
{
    QMessageBox::about(this,PROG_NAME,QString("<html><font size=16><b>%1 %2</b></font><br>"
                       "Работа с репозиториями git для MSSQL<br><br>"
                       "Основан на: <ul><li>Qt (v%3) </li><li>GCC (v%4.%5.%6)</li><li>libgit2 (v%7)</li><li>%8</li></ul>%9<br>"
                       "<p>Собран: %10 %11 </p>"
                       "<p>&copy; %12 <a href=mailto:nastvood@gmail.com>nastvood@gmail.com</a><p>"
                       "<p>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE "
                        " WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</p></html>")
                       .arg(PROG_NAME)
                       .arg(VERSION_STR)
                       .arg(QT_VERSION_STR)
                       .arg(__GNUC__).arg(__GNUC_MINOR__).arg(__GNUC_PATCHLEVEL__)
                       .arg(gGit::getLibGit2Version())
                       .arg(gCrypt::getCryptLibraryVersion())
                       .arg(git.getVersion())
                       .arg(__DATE__)
                       .arg(__TIME__)
                       .arg(QDate::currentDate().year())
                );
}

void MainWindow::appendRecentlyAction(const QString &projectPath)
{
    QHash<QString, QVariant> hash=recentlySettings->value("list").toHash();

    QList<QAction *> actions=ui->menuRecently->actions();
    if (!actions.isEmpty()){
        if (actions.count()>RECENTLY_ACTION_NUMBERS){
            QAction *action=actions.at(RECENTLY_ACTION_NUMBERS);
            QString actionText=action->text();

            hash.remove(actionText);
            recentlySettings->setValue("list",hash);

            ui->menuRecently->removeAction(action);
        }
    }

    if (!hash.contains(projectPath)){
        hash[projectPath]=projectPath;
        recentlySettings->setValue("list",hash);

        QAction *action=new QAction(projectPath,this);
        connect(action,SIGNAL(triggered()),SLOT(openRecentlyProject()));

        if (ui->menuRecently->actions().first())
            ui->menuRecently->insertAction(ui->menuRecently->actions().first(),action);
        else
            ui->menuRecently->addAction(action);
    }else{
        hash.remove(projectPath);
        hash[projectPath]=projectPath;
        recentlySettings->setValue("list",hash);
    }
}

bool MainWindow::isOpenedProject(const QString &projectName,const QString &projectPath)
{
    QList<QMdiSubWindow *> projects=area->subWindowList();
    for (int i=0;i<projects.count();i++){
        gProject *project=qobject_cast<gProject *>(projects.at(i)->widget());
        if (project){
            if ((projectName==project->getName()) && (projectPath==project->getPath()))
                return true;
        }
    }

    return false;
}


