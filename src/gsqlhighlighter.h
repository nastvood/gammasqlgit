#ifndef GSQLHIGHLIGHTER_H
#define GSQLHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextDocument>
#include <QDebug>

class gSQLHighlighter:public QSyntaxHighlighter
{
    Q_OBJECT

    public:
        gSQLHighlighter(QTextDocument *parent);

    private:
        QStringList keyWords;
        void iniKeyWords();
        inline void setFormatKeysWords(const QString &word, int pos);

    protected:
        void highlightBlock(const QString &text);

};
#endif // GSQLHIGHLIGHTER_H
