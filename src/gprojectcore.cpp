#include "gprojectcore.h"

gProjectCore::gProjectCore(DlgConnectionsStore *connectionsStore_, QObject *parent) :
    QObject(parent),
    connectionsStore(connectionsStore_)
{
    git=new gGit(this);
}

gProjectCore::~gProjectCore()
{
    clearConnections();
}

bool gProjectCore::open(const QString &path_,const QString &name_)
{
    path=path_;
    name=name_;

    QDir dir(path);
    dir.cd(name);
    git->init(dir.path());

    updateConnections();

    return true;
}

bool gProjectCore::checkConnectionInfrastructure(const QString &host,const QString &dbName)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name);

    QDir dirHost(dir.path());
    if (!dirHost.exists(host)){
        if (!dirHost.mkdir(host))
            return false;
    }

    dirHost.cd(host);
    if (!dirHost.exists(dbName)){
        if (!dirHost.mkdir(dbName))
            return false;
    }

    dirHost.cd(dbName);

    if (!dirHost.exists(getDirNameByObjType(GDB_P)))
        dirHost.mkdir(getDirNameByObjType(GDB_P));
    if (!dirHost.exists(getDirNameByObjType(GDB_F)))
        dirHost.mkdir(getDirNameByObjType(GDB_F));
    if (!dirHost.exists(getDirNameByObjType(GDB_T)))
        dirHost.mkdir(getDirNameByObjType(GDB_T));
    if (!dirHost.exists(getDirNameByObjType(GDB_V)))
        dirHost.mkdir(getDirNameByObjType(GDB_V));

    return true;
}

void gProjectCore::getListOfSQLObjects(const QString &host,const QString &dbName,gListSQlObjts &objects, gObjectDBType objType)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.cd(getDirNameByObjType(objType));

    gConnection *connection=connections.findDBByHostDB(host,dbName);
    if (!connection)
        return;

    connection->getListOfSQLObjcts(objects,objType);

    QStringList objFiles=dir.entryList(QDir::Files,QDir::Name);
    objFiles=objFiles.replaceInStrings(QRegExp(".sql$"),"");
    for (int i=0;i<objects.count();i++){
        if (objFiles.indexOf(objects[i].name+"."+objects[i].schema)>-1)
            objects[i].checked=true;
    }
}

void gProjectCore::updateObjects(const QString &host,const QString &dbName,const QStringList &selObjects,
                                 const gListSQlObjts &,gObjectDBType objType)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.cd(getDirNameByObjType(objType));

    QStringList objFiles=dir.entryList(QStringList()<<"*.sql",QDir::Files,QDir::Name);
    objFiles.replaceInStrings(".sql","");
    for (int i=0;i<selObjects.count();i++){
        QString objName=selObjects.at(i);
        int index=objFiles.indexOf(objName);
        if (index>-1){
            updateObject(host,dbName,objName,objType);
            objFiles.removeAt(index);
        }else{
            gConnection *connection=connections.findDBByHostDB(host,dbName);
            if (!connection)
                return;

            QByteArray objText=connection->getObjText(objName,objType);
            QFile file(dir.path()+"/"+objName+".sql");
            if (file.open(QIODevice::WriteOnly)){
                file.write(objText);
                emit sigAddObj(host,dbName,objName,objType);
                file.close();
            }
        }
    }

    for (int i=0;i<objFiles.count();i++){
        removeObject(host,dbName,objFiles.at(i),objType);
    }
}

void gProjectCore::updateObjects(const QString &host,const QString &dbName,gObjectDBType objType)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.cd(getDirNameByObjType(objType));

    QStringList objFiles=dir.entryList(QStringList()<<"*.sql",QDir::Files,QDir::Name);
    objFiles.replaceInStrings(".sql","");
    for (int i=0;i<objFiles.count();i++){
        QString objName=objFiles.at(i);
        updateObject(host,dbName,objName,objType);
    }
}

void gProjectCore::updateObject(const QString &host, const QString &dbName, const QString &objName, gObjectDBType objType, bool *removed)
{
    if (removed)
        *removed=false;

    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.cd(getDirNameByObjType(objType));

    emit sigMessActionText("Начинаю обновлять "+objName);
    gConnection *connection=connections.findDBByHostDB(host,dbName);
    if (!connection)
        return;

    QByteArray objText=connection->getObjText(objName,objType);

    if(objText.isEmpty()){
        removeObject(host,dbName,objName,objType);
        if (removed)
            *removed=true;

        emit sigUpdateObject(host,dbName,objType,objName,GUT_REMOVE);

        return;
    }else{
        QFile file(dir.path()+"/"+objName+".sql");
        if (file.open(QIODevice::ReadOnly)){
            QByteArray fileContent=file.readAll();
            file.close();

            if (!gHelper::isEqualByteArray(objText,fileContent)){
                if (file.open(QIODevice::WriteOnly)){
                    file.write(objText);
                    file.close();

                    emit sigUpdateObject(host,dbName,objType,objName,GUT_UPDATE);
                }
            }else
                emit sigUpdateObject(host,dbName,objType,objName,GUT_NONE);
        }
    }

    emit sigMessActionText("Закончил обновлять "+objName);
}

void gProjectCore::removeObject(const QString &host, const QString &dbName,const QString &selObj, gObjectDBType objType)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.cd(getDirNameByObjType(objType));

    emit sigMessActionText("Начинаю удалять "+selObj);

    QFile file(dir.path()+"/"+selObj+".sql");

    if (!file.exists())
            return;

    file.remove();
    emit sigRemoveObj(host,dbName,selObj,objType);
    emit sigMessActionText("Закончил удалять "+selObj);
}

void gProjectCore::removeObjects(const QString &host, const QString &dbName, gObjectDBType objType)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.cd(getDirNameByObjType(objType));

    QStringList entryList=dir.entryList(QDir::Files,QDir::Name|QDir::Reversed);

    for (int i=0;i<entryList.count();i++){
        QString objFileName=entryList.at(i);
        QString objName=objFileName.remove(objFileName.length()-4,4);

        removeObject(host,dbName,objName,objType);
    }
}

/*QByteArray gProjectCore::getTextObjectByName(const QString &objName, gObjectDBType objType)
{
    return db->getObjText(objName,objType);
}*/

QString gProjectCore::getDirNameByObjType(gObjectDBType objType)
{
    switch (objType) {
        case GDB_P:
            return "procs";
        case GDB_F:
            return "funcs";
        case GDB_T:
            return "tables";
        case GDB_V:
            return "views";
        default:
            return "others";
    }
}

gObjectDBType gProjectCore::getObjTypeByDirName(const QString &dirName)
{
    if (dirName=="procs"){
        return GDB_P;
    }else if (dirName=="funcs"){
        return GDB_F;
    }else if (dirName=="tables"){
        return GDB_T;
    }else if (dirName=="views"){
        return GDB_V;
    }

    return GDB_NONE;
}

QString gProjectCore::getDiff(const QString &file)
{
    return git->getDiff(file);
}

QString gProjectCore::getStatus(const QString &file)
{
    return git->getStatus(file);
}

BranchDescrList gProjectCore::getListOfBranches()
{
    return git->getListOfBranches();
}

void gProjectCore::setBranche(const QString &branche,git_branch_t type)
{
    git->setBranche(branche,type);
}

const gDescriptionConnections *gProjectCore::getDescriptionConnections()
{
    return &descriptionConnections;
}

void gProjectCore::updateAll()
{
    QString host;
    QString dbName;
    for (int i=0;i<connections.count();i++){
        host=connections.at(i)->host;
        dbName=connections.at(i)->dbName;

        updateObjects(host,dbName,GDB_P);
        updateObjects(host,dbName,GDB_F);
        updateObjects(host,dbName,GDB_T);
        updateObjects(host,dbName,GDB_V);
    }
}

void gProjectCore::updateAllHost(const QString &host,const QString &dbName)
{
    updateObjects(host,dbName,GDB_P);
    updateObjects(host,dbName,GDB_F);
    updateObjects(host,dbName,GDB_T);
    updateObjects(host,dbName,GDB_V);
}

void gProjectCore::updateConnections()
{
    clearConnections();

    descriptionConnections=connectionsStore->getDescriptionConnections();

    for (int i=0;i<descriptionConnections.count();i++){
        gDescriptionConnection descriptionConnection=descriptionConnections.at(i);

        gConnection *connection=new gConnection(descriptionConnection);
        connections.append(connection);

        checkConnectionInfrastructure(connection->host,connection->dbName);
    }
}

void gProjectCore::repositoryReopen()
{
    git->repositoryReopen();
}

void gProjectCore::clearConnections()
{
    for (int i=0;i<connections.count();i++){
        gConnection *connection=connections.at(i);
        connection->~gConnection();
    }

    connections.clear();
}


