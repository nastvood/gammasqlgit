#include "mainwindow.h"

#include <QApplication>
#include <QTextCodec>

int main(int argc, char *argv[])
{
    #if QT_VERSION < 0x050000
        QTextCodec *codec=QTextCodec::codecForName("UTF-8");
        QTextCodec::setCodecForCStrings(codec);
        QTextCodec::setCodecForLocale(codec);
        QTextCodec::setCodecForTr(codec);
    #endif

    QApplication a(argc, argv);

    MainWindow w;
    w.showMaximized();

    return a.exec();
}
