#ifndef GHELPER_H
#define GHELPER_H

#include <QStringList>
#include <QByteArray>
#include <QCryptographicHash>

class gHelper {
    public:
        static bool isEqualByteArray(const QByteArray &ba0,const QByteArray &ba1){
            return (QCryptographicHash::hash(ba0,QCryptographicHash::Md5)==QCryptographicHash::hash(ba1,QCryptographicHash::Md5));

        }

        static QByteArray createDbConnectionName(){
            QByteArray name=QString(QDateTime::currentDateTime().toString("dd.mm.yyyy hh:mm:ss.zzzz")+QString().setNum(qrand())).toLocal8Bit();
            return QCryptographicHash::hash(name,QCryptographicHash::Md5).toHex();
        }

        template <class T>
        static QList<T> reverseList(QList<T> lst){
            if (lst.size()>2){
                int lstSize = lst.size();
                for(int k = 0; k < lstSize/2; k++)
                    lst.swap(k,lstSize-(1+k));
            }

            return lst;
        }

        static QStringList reverseStringList(const QStringList &lst){
            return reverseList<QString>(lst);
        }
};

#endif // GHELPER_H
