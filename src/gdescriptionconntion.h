#ifndef GDESCRIPTIONCONNTION_H
#define GDESCRIPTIONCONNTION_H

#include <QString>
#include <QVector>

#include "gdb.h"
#include "ghelper.h"

class gDescriptionConnection
{
    public:
        gDescriptionConnection(const QString &host_, unsigned short port_, const QString &db_,
                            const QString &user_,const QString &passwd_,const QString &textCodec_,const QString &dbType_);

        gDescriptionConnection();
        QString host;
        unsigned short port;
        QString dbName;
        QString user;
        QString passwd;
        QString textCodec;
        QString dbType;
        bool empty;
};

typedef QVector<gDescriptionConnection> gDescriptionConnections;

class gConnection:public gDescriptionConnection,public gDB
{
    public:
        gConnection(gDescriptionConnection &descriptionConnection);
        ~gConnection();
};

class gPConnections : public QVector<gConnection*>
{
    public:
        gConnection *findDBByHostDB(const QString &host, const QString &dbName);
};

#endif // GDESCRIPTIONCONNTION_H
