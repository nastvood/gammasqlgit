#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QFile>
#include <QDateTime>
#include <QMdiArea>
#include <QSignalMapper>
#include <QMdiSubWindow>
#include <QMessageBox>
#include <QSettings>

#include "gproject.h"
#include "dlgcreateproject.h"
#include "gversion.h"
#include "dlgglobalsettings.h"

//кол-во ссыдко в меню недавно открытых проектов
#define RECENTLY_ACTION_NUMBERS 10

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    public slots:
        void setActiveSubWindow(QWidget *window);
        void showDlgCreateProject();
        void showDlgOpenProject();
        void showDlgGlobalSettings();
        void showAbout();
        void openRecentlyProject();
        void appendRecentlyAction(const QString &projectPath);

    private:
        Ui::MainWindow *ui;
        QMdiArea *area;
        QSignalMapper *mapper;
        DlgCreateProject *dlgCreateProject;
        DlgGlobalSettings *globalSettings;
        gGit git;
        QSettings *recentlySettings;

        QAction *actCreateProject;
        QAction *actOpenProject;
        QAction *actGlSettings;
        QAction *actClose;
        QAction *actAbout;

        bool isOpenedProject(const QString &projectName, const QString &projectPath);
        void openProject(const QString &projectName, const QString &projectPath, const QString &projectPassword, bool create=true);

        void removeRecentlySetting(QStringList &paths);
};

#endif // MAINWINDOW_H
