﻿#include "dlgselobj.h"
#include "ui_dlgselobj.h"

#define COL_NAME 0
#define COL_SCHEMA 1
#define COL_CREATE_AT 2
#define COL_UPDATE_AT 3

DlgSelObj::DlgSelObj(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgSelObj)
{
    ui->setupUi(this);

    ui->cmbMatch->addItem("Содержит",Qt::MatchContains);
    ui->cmbMatch->addItem("Начинается с",Qt::MatchStartsWith);
    ui->cmbMatch->addItem("Точное",Qt::MatchExactly);
    ui->cmbMatch->addItem("Регулярное выражение",Qt::MatchRegExp);
}

DlgSelObj::~DlgSelObj()
{
    delete ui;
}

QStringList DlgSelObj::getSelObj(const gListSQlObjts &objects,bool *ok)
{
    DlgSelObj dlgSelObj;
    dlgSelObj.setData(objects);

    QStringList listSelObjects;
    if (dlgSelObj.exec()==QDialog::Accepted){
        QTableWidgetItem *itemName;
        QTableWidgetItem *itemSchema;
        QTableWidget *tableOfObjects=dlgSelObj.getTableOfObjects();
        for (int i=0;i<tableOfObjects->rowCount();i++){
            itemName=tableOfObjects->item(i,COL_NAME);
            itemSchema=tableOfObjects->item(i,COL_SCHEMA);
            if (itemName->checkState()==Qt::Checked){
                listSelObjects.append(itemName->text()+"."+itemSchema->text());
            }
        }
        *ok=true;
    }else{
        *ok=false;
    }

    return listSelObjects;
}

void DlgSelObj::on_pbCancel_clicked()
{
    reject();
}

void DlgSelObj::on_pbUpdate_clicked()
{
    accept();
}

void DlgSelObj::setData(const gListSQlObjts &objects)
{
    ui->twObjects->clear();
    ui->twObjects->setColumnCount(4);

    ui->twObjects->setHorizontalHeaderLabels(QStringList()<<"Наименование"<<"Схема"<<"Дата создания"<<"Дата изм.");

    gSQLObj obj;
    for (int i=0;i<objects.count();i++){
        ui->twObjects->setRowCount(i+1);
        obj=objects.at(i);

        QTableWidgetItem *itemName=new QTableWidgetItem(obj.name);
        itemName->setCheckState(obj.checked?Qt::Checked:Qt::Unchecked);
        ui->twObjects->setItem(i,COL_NAME,itemName);

        QTableWidgetItem *itemSchema=new QTableWidgetItem(obj.schema);
        ui->twObjects->setItem(i,COL_SCHEMA,itemSchema);

        QTableWidgetItem *itemCreateAt=new QTableWidgetItem(obj.createAt.toString("dd.MM.yy hh:mm:ss"));
        ui->twObjects->setItem(i,COL_CREATE_AT,itemCreateAt);

        QTableWidgetItem *itemUpadateAt=new QTableWidgetItem(obj.updateAt.toString("dd.MM.yy hh:mm:ss"));
        ui->twObjects->setItem(i,COL_UPDATE_AT,itemUpadateAt);
    }
}

QTableWidget *DlgSelObj::getTableOfObjects()
{
    return ui->twObjects;
}

void DlgSelObj::resizeEvent(QResizeEvent*)
{
    ui->twObjects->setColumnWidth(COL_CREATE_AT,120);
    ui->twObjects->setColumnWidth(COL_UPDATE_AT,120);
    ui->twObjects->setColumnWidth(COL_NAME,
                                  ui->twObjects->width()-240-ui->twObjects->verticalHeader()->width()-25-ui->twObjects->columnWidth(COL_SCHEMA));
}

void DlgSelObj::showEvent(QShowEvent*)
{
    ui->leFind->clear();
    ui->cmbMatch->setCurrentIndex(0);
}

void DlgSelObj::on_leFind_textEdited(const QString&)
{
    QList<QTableWidgetItem *> items=ui->twObjects->findItems(ui->leFind->text(),
                                                             (Qt::MatchFlag)ui->cmbMatch->itemData(ui->cmbMatch->currentIndex()).toInt());
    if (!items.isEmpty()){
        ui->twObjects->setCurrentItem(items.at(0));
    }
}
