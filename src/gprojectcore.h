#ifndef _GPROJECTCORE_H
#define _GPROJECTCORE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QFile>
#include <QCryptographicHash>
#include <QTableWidgetItem>
#include <QDateTime>
#include <QDir>

#include "ggit.h"
#include "gdb.h"
#include "gsettings.h"
#include "gHelper.h"
#include "gdescriptionconntion.h"
#include "dlgconnectionsstore.h"

enum gObjectUpdateType {GUT_NONE=-1,GUT_UPDATE=1,GUT_REMOVE=2};

class gProjectCore : public QObject
{
    Q_OBJECT

    public:
        explicit gProjectCore(DlgConnectionsStore *connectionsStore_, QObject *parent = 0);
        ~gProjectCore();
        bool open(const QString &path_, const QString &name_);
        void getListOfSQLObjects(const QString &host, const QString &dbName, gListSQlObjts &objects, gObjectDBType objType);
        void updateObjects(const QString &host, const QString &dbName, const QStringList &selObj, const gListSQlObjts &, gObjectDBType objType);
        void updateObjects(const QString &host, const QString &dbName, gObjectDBType objType);
        void updateObject(const QString &host, const QString &dbName, const QString &objName, gObjectDBType objType,bool *removed=NULL);
        void removeObject(const QString &host, const QString &dbName, const QString &selObj, gObjectDBType objType);
        void removeObjects(const QString &host, const QString &dbName, gObjectDBType objType);
        //QByteArray getTextObjectByName(const QString &objectName, gObjectDBType objType);
        QString getDirNameByObjType(gObjectDBType objType);
        gObjectDBType getObjTypeByDirName(const QString &dirName);
        QString getDiff(const QString &file);
        QString getStatus(const QString &file);
        BranchDescrList getListOfBranches();
        void setBranche(const QString &branche, git_branch_t type);
        const gDescriptionConnections *getDescriptionConnections();
        void updateAll();
        void updateAllHost(const QString &host, const QString &dbName);
        void updateConnections();
        void repositoryReopen();

    signals:
        void sigRemoveObj(const QString &host, const QString &dbName, const QString &objectName, gObjectDBType objType);
        void sigAddObj(const QString &host, const QString &dbName, const QString &objectName, gObjectDBType objType);
        void sigMessActionText(const QString &mess);
        void sigUpdateObject(const QString &host, const QString &dbName,gObjectDBType objType,const QString &objectName,gObjectUpdateType updateType);

    public slots:

    private:
        QString path;
        QString name;
        gGit *git;
        DlgConnectionsStore *connectionsStore;
        gDescriptionConnections descriptionConnections;
        gPConnections connections;
        void clearConnections();
        bool checkConnectionInfrastructure(const QString &host, const QString &dbName);
};

#endif // GPROJECT_H
