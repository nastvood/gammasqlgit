﻿#include "gsettings.h"
#include "ui_gsettings.h"

gSettings::gSettings(const QString &nameProject_,const QString &pathProject_, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::gSettings)
{
    ui->setupUi(this);

    echoPassValue="default_value_line_edit";
    nameProject=nameProject_;
    pathProject=pathProject_;
}

gSettings::~gSettings()
{
    delete ui;
}

QString gSettings::getProjectPassword() const
{
    return projectPass;
}

void gSettings::setProjectPass(const QString &value)
{
    if (!projectPass.isEmpty())
        emit sigChangeProjectPassword(projectPass,value);

    projectPass=value;
}

void gSettings::initSettings()
{
}

void gSettings::showEvent(QShowEvent *)
{
    ui->lePassword->clear();
    ui->leRepeatPassword->clear();
}

void gSettings::on_pbCancel_clicked()
{
    close();
}

void gSettings::on_pbSave_clicked()
{
    if ((!ui->lePassword->text().isEmpty()) && (!ui->lePassword->text().isEmpty())){
        if (ui->lePassword->text()!=ui->lePassword->text()){
            QMessageBox::information(this,PROG_NAME,"Пароли не совпадают!");
        }else{
            setProjectPass(ui->lePassword->text());
        }
    }

    close();
}
