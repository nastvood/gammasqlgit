#ifndef DLGSELOBJ_H
#define DLGSELOBJ_H

#include <QDialog>
#include <QDebug>
#include <QStringList>
#include <QTableWidget>

#include "gsqlobj.h"

namespace Ui {
    class DlgSelObj;
}

class DlgSelObj : public QDialog
{
    Q_OBJECT
    
    public:
        explicit DlgSelObj(QWidget *parent = 0);
        ~DlgSelObj();
        QStringList getSelObj(const gListSQlObjts &objects, bool *ok=NULL);

    private slots:
        void on_pbCancel_clicked();
        void on_pbUpdate_clicked();
        void on_leFind_textEdited(const QString &);

    private:
        Ui::DlgSelObj *ui;
        void setData(const gListSQlObjts &objects);
        QTableWidget *getTableOfObjects();
        void resizeEvent(QResizeEvent *);
        void showEvent(QShowEvent *);

};

#endif // DLGSELOBJ_H
