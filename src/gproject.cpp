#include "gproject.h"
#include "ui_gproject.h"

gProject::gProject(const QString &nameProject, const QString &pathProject, DlgGlobalSettings * const globalSettings_, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::gProject),
    name(nameProject),
    path(pathProject)
{
    ui->setupUi(this);

    globalSettings=globalSettings_;

    cmbBranches=new QComboBox(this);
    connect(cmbBranches,SIGNAL(currentIndexChanged(QString)),SLOT(changeBranche(QString)));
    lblBranches=new QLabel("Ветки ",this);

    const int tabStop=4;
    QFont font=ui->teFile->font();
    QFontMetrics metrics(font);
    ui->teFile->setTabStopWidth(tabStop*metrics.width(' '));

    ui->splitterHor->setStretchFactor(0,3);
    ui->splitterHor->setStretchFactor(1,5);
    ui->splitterVert->setStretchFactor(1,7);
    ui->splitterVert->setStretchFactor(2,3);

    setWindowTitle(nameProject+" ["+pathProject+"]");

    setAttribute(Qt::WA_DeleteOnClose);

    dlgSelObj=new DlgSelObj(this);
    settings=new gSettings(nameProject,pathProject,this);
    connect(settings,SIGNAL(sigChangeProjectPassword(QString,QString)),SLOT(slotChangePasswordProject(QString,QString)));

    connectionsStore=new DlgConnectionsStore(nameProject,pathProject,this);
    connect(connectionsStore,SIGNAL(sigNewConnection(QString,QString)),SLOT(slotNewConnection(QString,QString)));
    connect(connectionsStore,SIGNAL(sigUpdateConnection(QString,QString,QString,QString)),SLOT(slotUpdateConnection(QString,QString,QString,QString)));
    connect(connectionsStore,SIGNAL(sigRemoveConnection(QString,QString)),SLOT(slotRemoveConnection(QString,QString)));

    core=new gProjectCore(connectionsStore,this);
    connect(core,SIGNAL(sigRemoveObj(QString,QString,QString,gObjectDBType)),SLOT(slotRemoveObject(QString,QString,QString,gObjectDBType)));
    connect(core,SIGNAL(sigAddObj(QString,QString,QString,gObjectDBType)),SLOT(slotAddObject(QString,QString,QString,gObjectDBType)));
    connect(core,SIGNAL(sigMessActionText(QString)),SLOT(messActionText(QString)));
    connect(core,SIGNAL(sigUpdateObject(QString,QString,gObjectDBType,QString,gObjectUpdateType)),SLOT(slotUpdateObject(QString,QString,gObjectDBType,QString,gObjectUpdateType)));

    sqlHighlighter=new gSQLHighlighter(ui->teFile->document());
    gitDiffHighlighter=new gGitDiffHighlighter(ui->teGitDiff->document());

    createToolBarMenu();
    createTreeContextMenu();

    connect(&processGitGui,SIGNAL(finished(int)),SLOT(finishedGitGui(int)));
}

gProject::~gProject()
{
    delete ui;
}

void gProject::release()
{
    this->~gProject();
}

bool gProject::openProject(const QString &password,bool create)
{
    QDir dir(path);
    if (!dir.exists())
        return false;

    if (!dir.exists(name)){
        if (!dir.mkdir(name))
            return false;
    }

    settings->setProjectPass(password);
    connectionsStore->init(password);

    core->open(path,name);

    if (create)
        connectionsStore->setPasswordCryptKey(password);
    else{
        if (!connectionsStore->checkPasswordCryptKey(password)){
            QMessageBox::warning(this,PROG_NAME,"Пароль проекта не верен!");
            return false;
        }
    }

    buildProjectTree();

    //show();
    adjustSize();

    updateCmbBranches();

    return true;
}

void gProject::openProjectDebug()
{
    QString pass="";

    settings->setProjectPass(pass);
    core->open(path,name);

    buildProjectTree();

    show();
    adjustSize();
}

QString gProject::getName()
{
    return name;
}

QString gProject::getPath()
{
    return path;
}

bool gProject::isValidProject(const QString &projectPath)
{
    QDir dir(projectPath);
    if (!dir.exists())
        return false;

    QString projectName=projectPath.mid(projectPath.lastIndexOf("\\")+1);
    if (projectName.isEmpty())
        return false;

    if (!dir.exists(projectName+".db"))
        return false;

    return true;
}

void gProject::buildProjectTree()
{
    ui->treeProject->clear();

    QString hostOld;
    QTreeWidgetItem *itemHost;
    const gDescriptionConnections *descriptionConnections=core->getDescriptionConnections();
    for(int i=0;i<descriptionConnections->count();i++){
        QDir dir(path);
        dir.cd(name);

        gDescriptionConnection connection=descriptionConnections->at(i);
        if (hostOld!=connection.host){
            itemHost=new QTreeWidgetItem((QTreeWidget*)0, QStringList()<<connection.host<<connection.dbType);
            ui->treeProject->insertTopLevelItem(0,itemHost);
            itemHost->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_HOST);
            itemHost->setData(0,PROJECT_ITEM_ROLE_HOST,connection.host);
            itemHost->setIcon(0,QIcon(":/img/host.png"));
            itemHost->setExpanded(true);

            hostOld=connection.host;
        }

        QTreeWidgetItem *itemDb=new QTreeWidgetItem(itemHost, QStringList()<<connection.dbName);
        itemDb->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_DB);
        itemDb->setData(0,PROJECT_ITEM_ROLE_HOST,connection.host);
        itemDb->setData(0,PROJECT_ITEM_ROLE_DB,connection.dbName);
        itemDb->setIcon(0,QIcon(":/img/db.png"));
        itemDb->setExpanded(true);

        dir.cd(connection.host);
        dir.cd(connection.dbName);
        QStringList entryList=dir.entryList(QDir::NoDotAndDotDot|QDir::Dirs|QDir::Files,QDir::Name|QDir::DirsLast);

        for (int i=0;i<entryList.count();i++){
            QTreeWidgetItem *item=new QTreeWidgetItem(itemDb, QStringList()<<entryList.at(i));

            if(QFileInfo(dir.path()+"/"+entryList.at(i)).isDir()){
                item->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_OBJECTS);
                item->setData(0,PROJECT_ITEM_ROLE_HOST,connection.host);
                item->setData(0,PROJECT_ITEM_ROLE_DB,connection.dbName);

                if(entryList.at(i)=="procs"){
                    item->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,GDB_P);
                    item->setIcon(0,QIcon(":/img/procs.png"));
                }
                else if(entryList.at(i)=="funcs"){
                    item->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,GDB_F);
                    item->setIcon(0,QIcon(":/img/funcs.png"));
                }
                else if(entryList.at(i)=="tables"){
                    item->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,GDB_T);
                    item->setIcon(0,QIcon(":/img/tables.png"));
                }
                else if(entryList.at(i)=="views"){
                    item->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,GDB_V);
                    item->setIcon(0,QIcon(":/img/views.png"));
                }
                else{
                    item->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,GDB_NONE);
                    item->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_UNKNOW);
                    item->setIcon(0,QIcon(":/img/dir.png"));
                }

                buildProjectTree(dir.path()+"/"+entryList.at(i),item);
            }else{
                item->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_UNKNOW);
                item->setIcon(0,QIcon(":/img/sql.png"));
            }
        }
    }
}

void gProject::buildProjectTree(const QString &path_, QTreeWidgetItem *item)
{
    QDir dir(path_);
    QStringList entryList=dir.entryList(QDir::NoDotAndDotDot|QDir::Dirs|QDir::Files,QDir::DirsFirst|QDir::Name);
    for (int i=0;i<entryList.count();i++){
        QTreeWidgetItem *subItem=new QTreeWidgetItem(item, QStringList()<<entryList.at(i));
        ui->treeProject->insertTopLevelItem(0,subItem);
        if(QFileInfo(dir.path()+"/"+entryList.at(i)).isDir()){
            subItem->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_UNKNOW);
            subItem->setIcon(0,QIcon(":/img/dir.png"));
            buildProjectTree(dir.path()+"/"+entryList.at(i),subItem);
        }else{
            if (((gProjectTreeItemType)item->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt()==GPTIT_OBJECTS) && (entryList.at(i).right(4)==".sql")){
                subItem->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_SCRIPT);
                subItem->setData(0,PROJECT_ITEM_ROLE_HOST,item->data(0,PROJECT_ITEM_ROLE_HOST));
                subItem->setData(0,PROJECT_ITEM_ROLE_DB,item->data(0,PROJECT_ITEM_ROLE_DB));
                subItem->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,item->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE));
                subItem->setIcon(0,QIcon(":/img/sql.png"));

                QString objectFileName=entryList.at(i);
                subItem->setText(0,objectFileName.remove(objectFileName.length()-4,4));
            }else{
                subItem->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_UNKNOW);
            }
        }
    }
}

void gProject::showSettings()
{
    settings->show();
}

void gProject::updateObjectsInDialog(gObjectDBType objType)
{
    QTreeWidgetItem *item=ui->treeProject->currentItem();
    if (!item){
        if (!item){
            QMessageBox::information(this,PROG_NAME,"Нет ни одного соединения!");
            return;
        }
    }

    bool foundDbItem=true;

    QVariant roleTypeRole=item->data(0,PROJECT_ITEM_ROLE_TYPE);
    if (!roleTypeRole.isValid())
        foundDbItem=false;

    QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
    QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
    if (!(hostRole.isValid() && dbNameRole.isValid()))
        foundDbItem=false;

    if (!foundDbItem){
        item=ui->treeProject->topLevelItem(0);

        if (!item->childCount())
            return;

        item=item->child(0);
        hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
        dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
    }

    QString host=hostRole.toString();
    QString dbName=dbNameRole.toString();

    gListSQlObjts objects;
    core->getListOfSQLObjects(host,dbName,objects,objType);
    bool ok;
    QStringList selObjects=dlgSelObj->getSelObj(objects,&ok);
    if (ok){
        core->updateObjects(host,dbName,selObjects,objects,objType);

        if ((gProjectTreeItemType)item->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt()==GPTIT_SCRIPT)
            selTreeItem(item);
    }
}

void gProject::showSPUpdate()
{
    updateObjectsInDialog(GDB_P);
}

void gProject::showFUpdate()
{
    updateObjectsInDialog(GDB_F);
}

void gProject::showTUpdate()
{
    updateObjectsInDialog(GDB_T);
}

void gProject::showVUpdate()
{
    updateObjectsInDialog(GDB_V);
}

void gProject::showGitGui()
{
    QString program=QString("\"%1\" \"%2\" --working-dir \"%3\"")
                    .arg(globalSettings->getTkInterpPath())
                    .arg(globalSettings->getGitGuiPath())
                    .arg(path+"/"+name);

    processGitGui.start(program);
}

void gProject::closeProject()
{
    this->parentWidget()->close();
}

void gProject::on_treeProject_customContextMenuRequested(const QPoint &pos)
{
    if (ui->treeProject->indexAt(pos).isValid()){
        QTreeWidgetItem *item=ui->treeProject->itemAt(pos);
        gProjectTreeItemType roleType=(gProjectTreeItemType)item->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt();
        if ((roleType==GPTIT_DB) || (roleType==GPTIT_OBJECTS) || (roleType==GPTIT_SCRIPT))
            contextMenuFileTree->exec(QCursor::pos());
    }
}

QString gProject::getPathByItem(QTreeWidgetItem *item_)
{
    QString itemPath;

    QTreeWidgetItem *item=item_;
    while (item){
        itemPath.prepend("/"+item->text(0));
        item=item->parent();
    }
    itemPath.prepend(path+"/"+name);

    return itemPath;
}

void gProject::updateAll()
{
    core->updateAll();

    QTreeWidgetItem *item=ui->treeProject->currentItem();
    if (item){
        if ((gProjectTreeItemType)item->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt()==GPTIT_SCRIPT)
            selTreeItem(item);
    }
}

void gProject::cntMnExplorer()
{
    QFileInfo info(getPathByItem(ui->treeProject->currentItem()));
    QDesktopServices::openUrl(QUrl(info.path()));
}

void gProject::cntMnUpdate()
{
    QList<QTreeWidgetItem *> items=ui->treeProject->selectedItems();
    if (!items.count())
        return;

    QTreeWidgetItem *item=items.first();
    QVariant roleTypeRole=item->data(0,PROJECT_ITEM_ROLE_TYPE);
    if (!roleTypeRole.isValid())
        return;

    gProjectTreeItemType itemType=(gProjectTreeItemType)roleTypeRole.toInt();
    switch (itemType) {
        case GPTIT_DB:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            if (!(hostRole.isValid() && dbNameRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();

            core->updateAllHost(host,dbName);
            break;
        }
        case GPTIT_OBJECTS:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            QVariant objectTypeRole=item->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE);
            if (!(hostRole.isValid() && dbNameRole.isValid() && objectTypeRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();
            gObjectDBType type=(gObjectDBType)objectTypeRole.toUInt();

            if(type!=GDB_NONE)
                core->updateObjects(host,dbName,type);

            break;
        }
        case GPTIT_SCRIPT:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            QVariant objectTypeRole=item->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE);
            if (!(hostRole.isValid() && dbNameRole.isValid() && objectTypeRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();
            gObjectDBType type=(gObjectDBType)objectTypeRole.toUInt();

            if(type!=GDB_NONE){
                QString objectName=item->text(0).replace(".sql","");
                bool removed;
                core->updateObject(host,dbName,objectName,type,&removed);
                if (!removed)
                    selTreeItem(item);
            }

            break;
        }
        default:
            break;
    }
}

void gProject::cntMnRemove()
{
    QTreeWidgetItem *item=ui->treeProject->currentItem();
    QVariant roleTypeRole=item->data(0,PROJECT_ITEM_ROLE_TYPE);
    if (!roleTypeRole.isValid())
        return;

    gProjectTreeItemType itemType=(gProjectTreeItemType)roleTypeRole.toInt();
    switch (itemType) {
        case GPTIT_DB:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            if (!(hostRole.isValid() && dbNameRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();

            for(int i=0;i<gDB::gObjectDBTypeList.count();i++){
                core->removeObjects(host,dbName,gDB::gObjectDBTypeList.at(i));
            }

            break;
        }
        case GPTIT_OBJECTS:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            QVariant objectTypeRole=item->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE);
            if (!(hostRole.isValid() && dbNameRole.isValid() && objectTypeRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();
            gObjectDBType type=(gObjectDBType)objectTypeRole.toUInt();

            int ret=QMessageBox::question(this,PROG_NAME,"Вы действительно желаете удалить скрипты?",QMessageBox::Yes,QMessageBox::No);
            if (ret==QMessageBox::Yes){
                if(type!=GDB_NONE){
                    core->removeObjects(host,dbName,type);
                }
            }

            break;
        }
        case GPTIT_SCRIPT:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            QVariant objectTypeRole=item->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE);
            if (!(hostRole.isValid() && dbNameRole.isValid() && objectTypeRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();
            gObjectDBType type=(gObjectDBType)objectTypeRole.toUInt();

            if(type!=GDB_NONE){
                int ret=QMessageBox::question(this,PROG_NAME,"Вы действительно желаете удалить скрипт?",QMessageBox::Yes,QMessageBox::No);
                if (ret==QMessageBox::Yes){
                    core->removeObject(host,dbName,item->text(0),type);
                    ui->teFile->clear();
                }
            }

            break;
        }
        default:
            break;
    }
}

QTreeWidgetItem *gProject::findItem(const QString &text, gProjectTreeItemType itemType,QTreeWidgetItem *parent)
{
    QList<QTreeWidgetItem *> foundItems=ui->treeProject->findItems(text,Qt::MatchExactly|Qt::MatchRecursive,0);
    for(QList<QTreeWidgetItem*>::iterator it = foundItems.begin();it != foundItems.end(); ++it){
        if (((*it)->parent()==parent) && ((gProjectTreeItemType)(*it)->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt()==itemType))
            return (*it);
    }

    return NULL;
}

QTreeWidgetItem *gProject::findItemObjects(const QString &host, const QString &dbName, gObjectDBType objType)
{
    QTreeWidgetItem *itemHost=findItem(host,GPTIT_HOST);
    if (!itemHost)
        return NULL;

    QTreeWidgetItem *itemDb=findItem(dbName,GPTIT_DB,itemHost);
    if (!itemDb)
        return NULL;

    QString objectsName=core->getDirNameByObjType(objType);
    return findItem(objectsName,GPTIT_OBJECTS,itemDb);
}

QTreeWidgetItem *gProject::findItemScript(const QString &host, const QString &dbName, gObjectDBType objectType, const QString &objectName)
{
    QTreeWidgetItem *itemObjects=findItemObjects(host,dbName,objectType);
    if (!itemObjects)
        return NULL;

    return findItem(objectName,GPTIT_SCRIPT,itemObjects);
}

void gProject::slotRemoveObject(const QString &host, const QString &dbName, const QString &objectName, gObjectDBType objType)
{
    QTreeWidgetItem *itemObjects=findItemObjects(host,dbName,objType);
    if (!itemObjects)
        return;

    QList<QTreeWidgetItem *> foundItems=ui->treeProject->findItems(objectName,Qt::MatchExactly|Qt::MatchRecursive,0);
    for(QList<QTreeWidgetItem*>::iterator it = foundItems.begin();it != foundItems.end(); ++it){
        if ((gProjectTreeItemType)(*it)->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt()==GPTIT_SCRIPT)
            delete (*it);
    }
}

void gProject::slotAddObject(const QString &host, const QString &dbName, const QString &objectName, gObjectDBType objType)
{
    QTreeWidgetItem *itemObjects=findItemObjects(host,dbName,objType);
    if (!itemObjects)
        return;

    QTreeWidgetItem *itemNew=new QTreeWidgetItem(QStringList()<<objectName);
    itemNew->setData(0,PROJECT_ITEM_ROLE_TYPE,GPTIT_SCRIPT);
    itemNew->setData(0,PROJECT_ITEM_ROLE_HOST,itemObjects->data(0,PROJECT_ITEM_ROLE_HOST));
    itemNew->setData(0,PROJECT_ITEM_ROLE_DB,itemObjects->data(0,PROJECT_ITEM_ROLE_DB));
    itemNew->setData(0,PROJECT_ITEM_ROLE_OBJECT_TYPE,itemObjects->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE));
    itemNew->setIcon(0,QIcon(":/img/sql.png"));

    itemObjects->addChild(itemNew);
    itemObjects->sortChildren(0,Qt::AscendingOrder);
}

void gProject::slotNewConnection(const QString &, const QString &)
{
    core->updateConnections();
    buildProjectTree();
}

void gProject::slotUpdateConnection(const QString &host, const QString &,
                                    const QString &hostOld, const QString &)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+hostOld+separator);
    dir.cdUp();
    dir.rename(hostOld,host);

    core->updateConnections();
    buildProjectTree();
}

void gProject::removeObjectsDir(const QString &host, const QString &dbName, gObjectDBType objType)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host+separator+dbName);
    dir.rmdir(core->getDirNameByObjType(objType));
}

void gProject::removeDbDir(const QString &host, const QString &dbName)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name+separator+host);
    dir.rmdir(dbName);
}

void gProject::removeHostDir(const QString &host)
{
    QChar separator=QDir::separator();
    QDir dir(path+separator+name);
    dir.rmdir(host);
}

void gProject::slotRemoveConnection(const QString &host, const QString &dbName)
{
    QTreeWidgetItem *itemHost=findItem(host,GPTIT_HOST);
    if (!itemHost)
        return;

    QTreeWidgetItem *itemDb=findItem(dbName,GPTIT_DB,itemHost);
    if (!itemDb)
        return;

    for(int i=0;i<gDB::gObjectDBTypeList.count();i++){
        core->removeObjects(host,dbName,gDB::gObjectDBTypeList.at(i));
        removeObjectsDir(host,dbName,gDB::gObjectDBTypeList.at(i));
    }
    removeDbDir(host,dbName);

    delete itemDb;

    if (!itemHost->childCount()){
        removeHostDir(host);
        delete itemHost;
    }

    //core->updateConnections();
}

void gProject::showConnectionsStore()
{
    connectionsStore->show();
}

void gProject::createTreeContextMenu()
{
    ui->treeProject->setContextMenuPolicy(Qt::CustomContextMenu);
    contextMenuFileTree=new QMenu(this);

    actCntMnExplorer=new QAction(QIcon(":/img/explorer.png"),"Проводник",this);
    contextMenuFileTree->addAction(actCntMnExplorer);
    connect(actCntMnExplorer,SIGNAL(triggered()),SLOT(cntMnExplorer()));

    actCntMnUpdate=new QAction(QIcon(":/img/update_t.png"),"Обновить",this);
    contextMenuFileTree->addAction(actCntMnUpdate);
    connect(actCntMnUpdate,SIGNAL(triggered()),SLOT(cntMnUpdate()));

    contextMenuFileTree->addSeparator();

    actCntMnRemove=new QAction(QIcon(":/img/remove.png"),"Удалить",this);
    contextMenuFileTree->addAction(actCntMnRemove);
    connect(actCntMnRemove,SIGNAL(triggered()),SLOT(cntMnRemove()));
}

void gProject::createToolBarMenu()
{
    ui->toolBar->addWidget(lblBranches);
    updateCmbBranches();
    ui->toolBar->addWidget(cmbBranches);
    ui->toolBar->addSeparator();

    actSettings=new QAction(QIcon(":/img/settings.png"),"Настройки",this);
    ui->toolBar->addAction(actSettings);
    connect(actSettings,SIGNAL(triggered()),SLOT(showSettings()));
    ui->toolBar->addSeparator();

    actSqlConnections=new QAction(QIcon(":/img/connections.png"),"Соединения",this);
    ui->toolBar->addAction(actSqlConnections);
    connect(actSqlConnections,SIGNAL(triggered()),SLOT(showConnectionsStore()));
    ui->toolBar->addSeparator();

    actFUpdate=new QAction(QIcon(":/img/funcs.png"),"Обновить функции",this);
    ui->toolBar->addAction(actFUpdate);
    connect(actFUpdate,SIGNAL(triggered()),SLOT(showFUpdate()));

    actSPUpdate=new QAction(QIcon(":/img/procs.png"),"Обновить хранимые процедуры",this);
    ui->toolBar->addAction(actSPUpdate);
    connect(actSPUpdate,SIGNAL(triggered()),SLOT(showSPUpdate()));

    actTUpdate=new QAction(QIcon(":/img/tables.png"),"Обновить таблицы",this);
    ui->toolBar->addAction(actTUpdate);
    connect(actTUpdate,SIGNAL(triggered()),SLOT(showTUpdate()));

    actVUpdate=new QAction(QIcon(":/img/views.png"),"Обновить представления",this);
    ui->toolBar->addAction(actVUpdate);
    connect(actVUpdate,SIGNAL(triggered()),SLOT(showVUpdate()));

    ui->toolBar->addSeparator();

    actUpdateAll=new QAction(QIcon(":/img/update.png"),"Обновить все",this);
    ui->toolBar->addAction(actUpdateAll);
    connect(actUpdateAll,SIGNAL(triggered()),SLOT(updateAll()));
    ui->toolBar->addSeparator();

    actGit=new QAction(QIcon(":/img/git.png"),"Git Gui",this);
    ui->toolBar->addAction(actGit);
    connect(actGit,SIGNAL(triggered()),SLOT(showGitGui()));

    ui->toolBar->addSeparator();
    actClose=new QAction(QIcon(":/img/close.png"),"Закрыть проект",this);
    ui->toolBar->addAction(actClose);
    connect(actClose,SIGNAL(triggered()),SLOT(closeProject()));
}
void gProject::changeBranche(const QString &branche)
{
    if (cmbBranches->currentData(BRANCH_ROLE_NAME).isValid()){
        git_branch_t type = (git_branch_t)cmbBranches->currentData(BRANCH_ROLE_TYPE).toInt();
        core->setBranche(branche,type);
        on_treeProject_itemSelectionChanged();
    }
}

void gProject::updateCmbBranches()
{
    cmbBranches->clear();

    BranchDescrList branches = core->getListOfBranches();
    for (int i=0;i<branches.count();i++){
        cmbBranches->insertItem(i,branches.at(i).name);
        cmbBranches->setItemData(i,branches.at(i).name,BRANCH_ROLE_NAME);
        cmbBranches->setItemData(i,branches.at(i).type,BRANCH_ROLE_TYPE);
    }
}

void gProject::selTreeItem(QTreeWidgetItem *item)
{
    ui->teFile->clear();
    ui->teGitDiff->clear();
    ui->leStatus->clear();

    if (!item->parent())
        return;

    QString pathFile=getPathByItem(item);
    if ((gProjectTreeItemType)item->data(0,PROJECT_ITEM_ROLE_TYPE).toUInt()==GPTIT_SCRIPT)
        pathFile.append(".sql");

    if (!QFileInfo(pathFile).isFile())
        return;

    QFile file(pathFile);
    if (!file.open(QIODevice::ReadOnly))
        return;

    ui->teFile->document()->setPlainText(file.readAll());
    file.close();

    QString relativeFilePath=pathFile.mid(QString(path+"/"+name+"/").length());
    ui->teGitDiff->document()->setPlainText(core->getDiff(relativeFilePath));
    ui->leStatus->setText(core->getStatus(relativeFilePath));
}


void gProject::on_treeProject_itemSelectionChanged()
{
    QTreeWidgetItem *item=ui->treeProject->currentItem();
    if(item){
        selTreeItem(item);
    }
}

void gProject::finishedGitGui(int)
{
    core->repositoryReopen();

    updateCmbBranches();
    if ((cmbBranches->count()>0) && (cmbBranches->currentIndex()==-1)){
        cmbBranches->setCurrentIndex(0);
        on_treeProject_itemSelectionChanged();
    }
}

void gProject::on_treeProject_doubleClicked(const QModelIndex &)
{
    QTreeWidgetItem *item=ui->treeProject->currentItem();
    QVariant roleTypeRole=item->data(0,PROJECT_ITEM_ROLE_TYPE);
    if (!roleTypeRole.isValid())
        return;

    gProjectTreeItemType itemType=(gProjectTreeItemType)roleTypeRole.toInt();

    switch (itemType) {
        case GPTIT_OBJECTS:{
            QVariant hostRole=item->data(0,PROJECT_ITEM_ROLE_HOST);
            QVariant dbNameRole=item->data(0,PROJECT_ITEM_ROLE_DB);
            QVariant objectTypeRole=item->data(0,PROJECT_ITEM_ROLE_OBJECT_TYPE);
            if (!(hostRole.isValid() && dbNameRole.isValid() && objectTypeRole.isValid()))
                return;

            QString host=hostRole.toString();
            QString dbName=dbNameRole.toString();
            gObjectDBType type=(gObjectDBType)objectTypeRole.toUInt();


            if(type!=GDB_NONE){
                if (!item->isExpanded())
                    item->setExpanded(true);

                updateObjectsInDialog(type);
            }
            break;
        }
        default:
            break;
    }
}

void gProject::messActionText(const QString &mess)
{
    emit sigMessActionText(mess);
}

void gProject::slotChangePasswordProject(const QString &oldPassword, const QString &newPassword)
{
    connectionsStore->changePasswordCryptKey(oldPassword,newPassword);
}

void gProject::slotUpdateObject(const QString &host, const QString &dbName,gObjectDBType objectType,
                                const QString &objectName, gObjectUpdateType updateType)
{
    QTreeWidgetItem *itemScript=findItemScript(host,dbName,objectType,objectName);
    if (!itemScript)
        return;

    switch (updateType) {
        case GUT_NONE:
            itemScript->setTextColor(0,QColor(0,0,0));
            break;
        case GUT_UPDATE:
            itemScript->setTextColor(0,QColor(20,150,20));//выделяем зелененьким если был изменен
            break;
        default:
            break;
    }
}
