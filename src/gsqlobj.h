#ifndef GSQLOBJ_H
#define GSQLOBJ_H

#include <QString>
#include <QDateTime>
#include <QVector>

enum gSQlObjType {SOT_SP=0};

struct gSQLObj;

typedef QVector<gSQLObj> gListSQlObjts;

struct gSQLObj{
    unsigned long long idObj;
    gSQlObjType type;
    QString name;
    QString schema;
    QDateTime createAt;
    QDateTime updateAt;
    bool checked;
};

#endif // GSQLOBJ_H
