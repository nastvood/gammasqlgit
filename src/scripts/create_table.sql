DECLARE @object_id INT,
                @schema_name SYSNAME,
                @table_name SYSNAME,
                @column_name SYSNAME,
                @data_type SYSNAME,
                @column_id INT,
                @max_length INT,
                @precision INT,
                @scale INT,
                @is_nullable INT,
                @collation_name SYSNAME,
                @is_identity BIT,
                @computed_column_definition VARCHAR(MAX),
                @constraint_type VARCHAR(10),
                @constraint_name SYSNAME,
                @definition VARCHAR(MAX),
                @constraint_number INT,
                @is_system_named BIT,
                @index_id INT,
                @key_desc VARCHAR(MAX),
                @type_desc VARCHAR(MAX),
                @fkey_id INT,
                @delete_referential_action BIT,
                @delete_referential_action_desc VARCHAR(MAX),
                @update_referential_action BIT,
                @update_referential_action_desc VARCHAR(MAX)


DECLARE @padding INT,
                @column_holder VARCHAR(MAX),
                @column_list VARCHAR(MAX),
                @create_construct VARCHAR(MAX),
                @create_holder VARCHAR(MAX),
                @constraint_holder VARCHAR(MAX),
                @constraint_list VARCHAR(MAX),
                @fkey_construct VARCHAR(MAX),
                @fkey_list VARCHAR(MAX),
                @referenced_column_list VARCHAR(MAX)

DECLARE @script_default_constraints BIT,
                @script_check_constraints BIT,
                @script_primary_and_unique_keys BIT,
                @script_foreign_keys BIT,
                @print_to_screen BIT,
                @alter_holder VARCHAR(MAX),
                @alter_construct VARCHAR(MAX),
                @key_construct VARCHAR(MAX),
                @sql VARCHAR(MAX)

DECLARE @br_line CHAR(2)

SET @br_line= CHAR(13) + CHAR(10)

SET @create_holder = 'CREATE TABLE [schema_name].[table_name] (
[column_list]'
--	)
--GO'

SET @alter_holder = '
ALTER TABLE [schema_name].[table_name] ADD [constraint_list]
GO
'

SET NOCOUNT ON

SELECT TOP(1)
                @object_id=t.object_id,
                @schema_name=QuoteName(s.name),
                @table_name=QuoteName(t.name)
        FROM sys.schemas s JOIN sys.tables t ON s.schema_id = t.schema_id
        WHERE t.name='{table_name}' AND s.name='{schema_name}'
        ORDER BY s.name, t.name


DECLARE column_cursor CURSOR FOR
        SELECT QuoteName(c.name) As [column_name],
                        x.name As [data_type],
                        c.column_id,
                        c.max_length,
                        c.precision,
                        c.scale,
                        c.is_nullable,
                        c.collation_name,
                        c.is_identity,
                        comp.definition As [computed_column_definition]
                FROM sys.columns c JOIN sys.types x ON c.user_type_id = x.user_type_id
                        LEFT JOIN sys.computed_columns comp ON c.object_id = comp.object_id AND c.column_id = comp.column_id
                WHERE c.object_id = @object_id
                ORDER BY c.column_id

        SELECT @padding = Max(Len(name))
                FROM   sys.columns
                WHERE  object_id = @object_id

        OPEN column_cursor

        FETCH NEXT FROM column_cursor
                INTO @column_name, @data_type, @column_id, @max_length, @precision, @scale, @is_nullable, @collation_name, @is_identity, @computed_column_definition

        WHILE @@FETCH_STATUS=0
        BEGIN
                SET @column_holder = @column_name+' '

                IF @computed_column_definition IS NOT NULL
                        SET @column_holder = @column_holder + 'As ' + @computed_column_definition
                ELSE
                BEGIN
                        SET @column_holder = @column_holder + QuoteName(@data_type)

                        --Character and binary datatypes
                        IF @max_length = -1
                                SET @column_holder = @column_holder + '(max)'
                        ELSE IF @data_type IN ('char', 'varchar', 'nchar', 'nvarchar', 'binary', 'varbinary')
                                SET @column_holder = @column_holder + '(' + Convert(varchar(10), @max_length) + ')'

                        --Numeric
                        IF @data_type IN ('decimal', 'numeric')
                                SET @column_holder = @column_holder + '(' + Convert(varchar(10), @precision) + ',' + Convert(varchar(10), @scale) + ')'

                        --Float
                        IF @data_type IN ('float')
                        BEGIN
                                IF @max_length = 8
                                        SET @column_holder = @column_holder + '(53)'
                                ELSE
                                        SET @column_holder = @column_holder + '(24)'
                        END

                        IF @is_identity = 1
                                SET @column_holder = @column_holder + ' identity(' + Convert(varchar(10), Ident_Seed(@table_name)) + ',' + Convert(varchar(10), Ident_Incr(@table_name)) + ')'

                        --Collation
                        --SET @column_holder = @column_holder + Coalesce(' COLLATE ' + @collation_name, '')

                        --nullability
                        IF @is_nullable = 0
                                SET @column_holder = @column_holder + ' NOT NULL'
                        ELSE
                                SET @column_holder = @column_holder + ' NULL'
                END

                IF @column_id = 1
                SET @column_list = CHAR(9)+@column_holder
                ELSE
                        SET @column_list = @column_list+',' + @br_line+CHAR(9)+@column_holder

                FETCH NEXT FROM column_cursor
                        INTO @column_name, @data_type, @column_id, @max_length, @precision, @scale, @is_nullable, @collation_name, @is_identity, @computed_column_definition
        END

CLOSE column_cursor
DEALLOCATE column_cursor

--Construct create command
SET @create_construct = @create_holder+','+@br_line
SET @create_construct = Replace(@create_construct, '[schema_name]', @schema_name)
SET @create_construct = Replace(@create_construct, '[table_name]', @table_name)
SET @create_construct = Replace(@create_construct, '[column_list]', @column_list)

DECLARE constraint_cursor CURSOR FOR
        SELECT constraint_type,
                         column_name,
                         constraint_name,
                         definition,
                         Row_Number() OVER (ORDER BY column_id) As [constraint_number],
                         is_system_named
                FROM (
                        SELECT 'DEFAULT' As [constraint_type],
                                         c.column_id,
                                         QuoteName(c.name) As [column_name],
                                         QuoteName(x.name) As [constraint_name],
                                         x.definition,
                                         x.is_system_named
                                FROM sys.default_constraints x JOIN sys.columns c ON (x.parent_object_id = c.object_id AND x.parent_column_id = c.column_id)
                                WHERE c.object_id = @object_id
                                        AND @script_default_constraints = 1
                        UNION ALL
                        SELECT 'CHECK' As [constraint_type],
                                        c.column_id,
                                        QuoteName(c.name) As [column_name],
                                        QuoteName(x.name) As [constraint_name],
                                        x.definition,
                                        x.is_system_named
                                FROM sys.check_constraints x JOIN sys.columns c ON (x.parent_object_id = c.object_id AND x.parent_column_id = c.column_id)
                                WHERE c.object_id = @object_id
                                        AND @script_check_constraints = 1
                ) x
                ORDER BY column_id

        SELECT @padding = COALESCE(Max(Len(name)),0)
                FROM sys.objects
        WHERE type IN ('C', 'D')
                        AND parent_object_id = @object_id

        SET @constraint_list = NULL

        OPEN constraint_cursor

        FETCH NEXT FROM constraint_cursor
                INTO @constraint_type, @column_name, @constraint_name, @definition, @constraint_number, @is_system_named

        WHILE @@FETCH_STATUS=0
        BEGIN
                SET @constraint_holder = 'CONSTRAINT '

                IF @is_system_named = 0
                        SET @constraint_holder = @constraint_holder + @constraint_name + Replicate(' ', @padding - Len(@constraint_name) + 3)

                SET @constraint_holder = @constraint_holder + @constraint_type + ' ' + @definition

                IF @constraint_type = 'DEFAULT'
                        SET @constraint_holder = @constraint_holder + ' FOR ' + @column_name

                IF @constraint_number = 1
                        SET @constraint_list = @constraint_holder
                ELSE
                        SET @constraint_list = @constraint_list + Char(13) + '  , ' + @constraint_holder

                FETCH NEXT FROM constraint_cursor
                        INTO @constraint_type, @column_name, @constraint_name, @definition, @constraint_number, @is_system_named
        END

CLOSE constraint_cursor
DEALLOCATE constraint_cursor

--Construct alter command
IF @constraint_list IS NOT NULL
BEGIN
        SET @alter_construct = @alter_holder
        SET @alter_construct = Replace(@alter_construct, '[schema_name]', @schema_name)
        SET @alter_construct = Replace(@alter_construct, '[table_name]', @table_name)
        SET @alter_construct = Replace(@alter_construct, '[constraint_list]', @constraint_list)
END
ELSE
        SET @alter_construct = ''


DECLARE key_cursor CURSOR FOR
        SELECT QuoteName(name) As [constraint_name],
                        type As [constraint_type],
                        unique_index_id As [constraint_number],
                        object_id As [index_id],
                        is_system_named
                FROM sys.key_constraints
                WHERE parent_object_id = @object_id
        ORDER BY object_id

        --Find padding
        SELECT @padding=Max(Len(name))
                FROM  sys.objects
                WHERE  type IN ('UQ', 'PK')
                        AND parent_object_id = @object_id

        SET @constraint_list = NULL

        OPEN key_cursor

        FETCH NEXT FROM key_cursor
                INTO @constraint_name, @constraint_type, @constraint_number, @index_id, @is_system_named

        WHILE @@FETCH_STATUS=0
        BEGIN
                SET @constraint_holder = ' CONSTRAINT '

                IF @is_system_named = 0
                        SET @constraint_holder = @constraint_holder + @constraint_name + Replicate(' ', @padding - Len(@constraint_name) + 3)

                IF @constraint_type = 'PK'
                        SET @constraint_holder = @constraint_holder + 'PRIMARY KEY'
                ELSE
                        SET @constraint_holder = @constraint_holder + 'UNIQUE '

                SELECT @type_desc=type_desc, @key_desc=' WITH ('+
                        CASE is_padded
                                WHEN 0 THEN 'PAD_INDEX  = OFF'
                                ELSE 'PAD_INDEX  = ON'
                        END+', '+
                        CASE no_recompute
                                WHEN 0 THEN 'STATISTICS_NORECOMPUTE  = OFF'
                                ELSE 'STATISTICS_NORECOMPUTE  = ON'
                        END+', '+
                        CASE allow_row_locks
                                WHEN 0 THEN 'ALLOW_ROW_LOCKS  = OFF'
                                ELSE 'ALLOW_ROW_LOCKS  = ON'
                        END+', '+
                        CASE allow_page_locks
                                WHEN 0 THEN 'ALLOW_ROW_LOCKS  = OFF'
                                ELSE 'ALLOW_ROW_LOCKS  = ON'
                        END
                        +') ON [PRIMARY]'
                        FROM sys.indexes i,sys.stats s
                        WHERE '['+i.name+']'=@constraint_name
                                AND '['+s.name+']'=@constraint_name

                SET @constraint_holder = @constraint_holder+' '+@type_desc+@br_line

                SET @column_list = ''

                --Find index columns
                SELECT @column_list=@column_list + QuoteName(c.name)+
                                CASE
                                        WHEN i.is_descending_key = 1 THEN ' DESC'
                                        ELSE ' ASC '
                                END	+ ', '
                        FROM sys.index_columns i JOIN sys.columns c ON (i.object_id = c.object_id AND i.index_column_id = c.column_id)
                        WHERE i.object_id = @object_id
                                AND i.index_id = @constraint_number
                        ORDER BY i.key_ordinal

                --Remove trailing comma (and space)
                SET @column_list = Left(@column_list, Len(@column_list) - 2)

                SET @constraint_holder = @constraint_holder + '('+@br_line+ @column_list + @br_line+')'+@key_desc

                IF @constraint_number = 1
                SET @constraint_list = @constraint_holder
                ELSE
                        SET @constraint_list = @constraint_list + ','+@br_line+ @constraint_holder

                FETCH NEXT FROM key_cursor
                        INTO @constraint_name, @constraint_type, @constraint_number, @index_id, @is_system_named
        END

CLOSE key_cursor
DEALLOCATE key_cursor

--Construct alter command
IF @constraint_list IS NOT NULL
BEGIN
        SET @key_construct = @constraint_list+@br_line+') ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'+@br_line+'GO'+@br_line
        --SET @key_construct = @alter_holder
        --SET @key_construct = Replace(@key_construct, '[schema_name]', @schema_name)
        --SET @key_construct = Replace(@key_construct, '[table_name]', @table_name)
        --SET @key_construct = Replace(@key_construct, '[constraint_list]', @constraint_list)
END
ELSE
        SET @key_construct = ''

--SET @sql = @create_construct+Coalesce(@alter_construct, '')+Coalesce(@key_construct, '')
SET @sql = @create_construct+Coalesce(@key_construct, '')

SET @fkey_list=''

DECLARE fkey_cursor CURSOR FOR
        SELECT QuoteName(name) As [constraint_name],
                        object_id As [fkey_id],
                        parent_object_id As [object_id],
                        delete_referential_action,
                        delete_referential_action_desc,
                        update_referential_action,
                        update_referential_action_desc
                FROM sys.foreign_keys
                WHERE parent_object_id=@object_id

        OPEN fkey_cursor

        FETCH NEXT FROM fkey_cursor
                INTO @constraint_name, @fkey_id, @object_id,@delete_referential_action,
                        @delete_referential_action_desc,@update_referential_action,@update_referential_action_desc

        WHILE @@FETCH_STATUS=0
        BEGIN
            SELECT @constraint_list = 'CONSTRAINT ',
                                @column_list =  '',
                                @referenced_column_list = '',
                                @schema_name = (SELECT s.name
                                                                        FROM   sys.schemas s JOIN sys.tables t ON s.schema_id = t.schema_id
                                                                                WHERE  t.object_id = @object_id)

                SELECT @column_list = @column_list + ', ' + QuoteName(p.name),
                                @referenced_column_list = @referenced_column_list + ', ' + QuoteName(r.name)
                        FROM sys.foreign_key_columns As [fk]JOIN sys.columns As [p] ON (fk.parent_object_id = p.object_id AND fk.parent_column_id = p.column_id)
                                JOIN sys.columns As [r] ON (fk.referenced_object_id = r.object_id AND fk.referenced_column_id = r.column_id)
                        WHERE constraint_object_id = @fkey_id

                SET @constraint_list = @constraint_list + @constraint_name
                        + ' FOREIGN KEY (' + Stuff(@column_list, 1, 2, '') + ')'
                        +@br_line+ 'REFERENCES ' + @schema_name + '.' + @table_name
                        + ' (' + Stuff(@referenced_column_list, 1, 2, '') + ')'+
                        CASE @delete_referential_action
                                                        WHEN 1 THEN @br_line+'ON DELETE '+@delete_referential_action_desc
                                                        ELSE ''
                                                END+
                                                CASE @update_referential_action
                                                        WHEN 1 THEN @br_line+'ON UPDATE '+@update_referential_action_desc
                                                        ELSE ''
                                                END

                --SET @fkey_construct = Stuff(@alter_holder, 1, 2, '') --Remove leading carriage return
                SET @fkey_construct = @alter_holder
                SET @fkey_construct = Replace(@fkey_construct, '[schema_name]', @schema_name)
                SET @fkey_construct = Replace(@fkey_construct, '[table_name]', @table_name)
                SET @fkey_construct = Replace(@fkey_construct, '[constraint_list]', @constraint_list)
                SET @fkey_construct = Replace(@fkey_construct, 'ADD CONSTRAINT', 'WITH CHECK ADD CONSTRAINT')

                SET @fkey_list=@fkey_list+@fkey_construct

                SET @fkey_construct = @alter_holder
                SET @fkey_construct = Replace(@fkey_construct, '[schema_name]', @schema_name)
                SET @fkey_construct = Replace(@fkey_construct, '[table_name]', @table_name)
                SET @fkey_construct = Replace(@fkey_construct, 'ADD', 'CHECK CONSTRAINT')
                SET @fkey_construct = Replace(@fkey_construct, '[constraint_list]',@constraint_name)

                SET @fkey_list=@fkey_list+@fkey_construct

                FETCH NEXT FROM fkey_cursor
                        INTO @constraint_name, @fkey_id, @object_id,@delete_referential_action,
                                @delete_referential_action_desc,@update_referential_action,@update_referential_action_desc
        END

CLOSE fkey_cursor
DEALLOCATE fkey_cursor

SET @sql = @sql+@fkey_list

--execute spWriteStringToFile @sql,'d:\','probe.sql'
--print @sql
SELECT @sql AS result




