DECLARE @definition as VARCHAR(MAX),
                @result as VARCHAR(MAX)

SET @result=''

SELECT RTRIM(definition) as definition
        INTO #triggers
    FROM sys.objects as strig JOIN sys.objects stable ON strig.parent_object_id=stable.object_id
                JOIN sys.sql_modules smodule ON strig.object_id=smodule.object_id
    WHERE strig.type='TR' AND
        stable.name='{table_name}' AND
        schema_name(strig.schema_id)='{schema_name}'

DECLARE triggers_cursor CURSOR FOR
        SELECT definition
                FROM #triggers

                OPEN triggers_cursor

        FETCH NEXT FROM triggers_cursor
                        INTO @definition

                WHILE @@FETCH_STATUS=0
        BEGIN
                        SET @result=@result+@definition+CHAR(10)+CHAR(13)

            FETCH NEXT FROM triggers_cursor
                                INTO @definition
        END



CLOSE triggers_cursor
DEALLOCATE triggers_cursor

SELECT RTRIM(@result) as result
