﻿#include "gdescriptionconntion.h"

gDescriptionConnection::gDescriptionConnection(const QString &host_, unsigned short port_,
                                           const QString &db_, const QString &user_, const QString &passwd_, const QString &textCodec_, const QString &dbType_)
    :host(host_),port(port_),dbName(db_),user(user_),passwd(passwd_),textCodec(textCodec_),dbType(dbType_)
{
    if (!host.isEmpty() && !dbName.isEmpty() && !user.isEmpty()){
        empty=false;
    } else
        empty=true;
}

gDescriptionConnection::gDescriptionConnection()
{}


gConnection::gConnection(gDescriptionConnection &descriptionConnection)
    :gDescriptionConnection(descriptionConnection)
{
    if (!empty){
        setDatabaseName(host,dbName,user,passwd);
    }
}

gConnection::~gConnection()
{}


gConnection *gPConnections::findDBByHostDB(const QString &host,const QString &dbName)
{
    for (int i=0;i<count();i++){
        gConnection *connection=at(i);
        if ((connection->host==host) && (connection->dbName==dbName)){
            return connection;
        }
    }

    return NULL;
}
