#include "ggit.h"

gGit::gGit(QObject *parent) :
    QObject(parent)
{
    ref=NULL;
    repo=NULL;
}

gGit::~gGit()
{
    if (repo){
        if (ref)
            git_reference_free(ref);

        git_repository_free(repo);
    }
}

void gGit::init(const QString path_)
{
    path=path_;

    QDir dir(path);
    if (!dir.exists(".git")){
        git_repository_init(&repo,dir.path().toLocal8Bit().data(),0);
    }else{
        if (GIT_OK==git_repository_open(&repo,dir.path().toLocal8Bit().data())){
            git_repository_head(&ref,repo);
            currentBranch=getCurrentBranch();
        }
    }
}

void gGit::repositoryReopen()
{
    if (repo){
        if (ref){
            git_reference_free(ref);
            ref=NULL;
        }

        git_repository_free(repo);
        repo=NULL;
    }

    QDir dir(path);
    if (GIT_OK==git_repository_open(&repo,dir.path().toLocal8Bit().data())){
        git_repository_head(&ref,repo);
    }

    currentBranch=getCurrentBranch();
}

QString gGit::getVersion()
{
    return runCommand("git --version");
}

QString gGit::getLibGit2Version()
{
    int major=0;
    int minor=0;
    int rev=0;
    git_libgit2_version(&major,&minor,&rev);

    return QString("%1.%2.%3").arg(major).arg(minor).arg(rev);
}

QString gGit::getDiff(const QString &file)
{
    QString strDiff;

    if (!repo)
        return strDiff;

    git_diff_options opts = GIT_DIFF_OPTIONS_INIT;
    char *filePath=new char[file.toLocal8Bit().length()+1];
    strncpy(filePath,file.toLocal8Bit().data(),file.toLocal8Bit().length());
    filePath[file.toLocal8Bit().length()]=0;
    opts.pathspec={&filePath,1};

    /*if (GIT_OK!=git_diff_index_to_workdir(&diff,repo,NULL,&opts)){
        delete[] filePath;
        return QString();
    }*/

    git_oid oid_parent_commit;
    if (git_reference_name_to_id(&oid_parent_commit,repo,"HEAD") == GIT_OK){
        git_commit * commit;
        if (git_commit_lookup(&commit, repo, &oid_parent_commit) == GIT_OK){
            git_tree *tree;
            if (git_commit_tree(&tree,commit) == GIT_OK){
                git_diff *diff;

                if (git_diff_tree_to_workdir(&diff,repo,tree,&opts) == GIT_OK){
                    git_diff_print(diff,GIT_DIFF_FORMAT_PATCH,gGit::callbackDiffData,&strDiff);
                    git_diff_free(diff);
                }

                git_tree_free(tree);
            }

            git_commit_free(commit);
        }
    }

    delete[] filePath;


    return strDiff;
}

int gGit::callbackDiffData(const git_diff_delta *delta, const git_diff_hunk *hunk,
                           const git_diff_line *line, void *payload)
{
    QString *diff=static_cast<QString *>(payload);

    if ((GIT_DIFF_LINE_ADDITION == line->origin) || (GIT_DIFF_LINE_DELETION == line->origin))
        diff->append(line->origin);

    if (line->content_len == 1){
            diff->append(QString(line->content).mid(0,line->content_len));
            return 0;
    }

    char *lineContent = new char[line->content_len + 1];

    strncpy(lineContent,line->content,line->content_len);
    lineContent[line->content_len] = '\0';

    QString content(lineContent);
    int break_pos = content.indexOf('\n');

    diff->append(QString(line->content).mid(0,break_pos + 1));

    delete[] lineContent;

    return 0;
}

QString gGit::getStatus(const QString &file)
{
    if (!repo)
        return QString();

    unsigned int status=0;
    if(git_status_file(&status,repo,file.toLocal8Bit().data()))
        return QString();

    QString fileStatus;

    if (status==GIT_STATUS_CURRENT)
        fileStatus.append(" без изменений ");
    else if (status & GIT_STATUS_INDEX_NEW)
        fileStatus.append(" подготовлено, новый файл ");
    else if (status & GIT_STATUS_INDEX_MODIFIED)
        fileStatus.append(" подготовлено, изменено ");
    else if (status & GIT_STATUS_WT_NEW)
        fileStatus.append(" не подготовлено, не отслеживается ");
    else if (status & GIT_STATUS_WT_MODIFIED)
        fileStatus.append(" не подготовлено, изменено ");

    return fileStatus;
}

QString gGit::getCurrentBranch()
{
    if (!repo)
        return QString();

    if (!ref)
        git_repository_head(&ref,repo);

    if (!ref)
        return QString();

    char *name=new char[256];
    git_branch_name((const char **)&name,ref);
    QString branchName(name);

    delete []name;

    return branchName;
}

BranchDescrList gGit::getListOfBranches()
{
    if (!repo)
        return BranchDescrList();

    if (!ref)
        git_repository_head(&ref,repo);

    if (!ref)
        return BranchDescrList();

    git_reference *ref_save = ref;

    git_branch_t branchType = GIT_BRANCH_ALL;
    git_branch_iterator *iter;
    if (git_branch_iterator_new(&iter,repo,branchType) != GIT_OK)
        return BranchDescrList();

    BranchDescrList branches;
    char *branchName;
    while (git_branch_next(&ref,&branchType,iter) == 0){
        if (git_branch_name((const char **)&branchName,ref) == GIT_OK){
            branches<<BranchDescr {QString(branchName).toLocal8Bit(),branchType};
        }
    }

    git_branch_iterator_free(iter);

    ref = ref_save;

    return branches;
}

void gGit::setBranche(const QString &branche,git_branch_t type)
{
    if (!repo)
        return;

    git_branch_lookup(&ref,repo,branche.toLocal8Bit().data(),type);
}

QString gGit::runCommand(const QString &command)
{
    QString result;

    QProcess proc;
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.setWorkingDirectory(path);
    proc.start(command);
    if (proc.waitForFinished())
        result=proc.readAll().trimmed();

    return result;
}
