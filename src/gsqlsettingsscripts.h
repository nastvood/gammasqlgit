#ifndef GSQLSETTINGSSCRIPTS_H
#define GSQLSETTINGSSCRIPTS_H

#define SQL_TABLE_CONNECTIONS "tbl_connections"
#define SQL_SCRIPT_CREATE_TABLE_CONNECTIONS "CREATE  TABLE tbl_connections (" \
                    "id_conn INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE ," \
                    "host VARCHAR(256) NOT NULL ," \
                    "port INTEGER NOT NULL ," \
                    "db VARCHAR(256) NOT NULL ," \
                    "user VARCHAR(256) NOT NULL, " \
                    "passwd VARCHAR(1024) NOT NULL, " \
                    "text_codec VARCHAR(128) NOT NULL, " \
                    "db_type VARCHAR(128) NOT NULL" \
                ")"

#define SQL_INDEX_TABLE_CONNECTIONS "connections_unique_index"
#define SQL_SCRIPT_CRAETE_INDEX_ON_TABLE_CONNECTIONS "CREATE UNIQUE INDEX 'connections_unique_index' ON 'tbl_connections' ('host','db','db_type' ASC);"

#define SQL_TABLE_CONNECTIONS_PASSWORD "tbl_conn_password"
#define SQL_SCRIPT_CREATE_TABLE_CONNECTIONS_PASSWORD "CREATE TABLE tbl_conn_password (" \
                    "id_conn_password INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE ," \
                    "password VARCHAR(1024) NOT NULL" \
                ")"
#define SQL_SCRIPT_INSERT_TABLE_CONNECTIONS_PASSWORD "INSERT INTO tbl_conn_password (password) VALUES ('')"

#endif // GSQLSETTINGSSCRIPTS_H
