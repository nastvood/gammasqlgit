﻿#include "gcrypt.h"

int gCrypt::repeated=10;

gCrypt::gCrypt()
{}

QByteArray gCrypt::encodePassword(const QString &password,const QByteArray &msg,bool *ok)
{
    QByteArray newMsg=msg.repeated(gCrypt::repeated);

    return gCrypt::encode(password,newMsg,ok);
}

QByteArray gCrypt::decodePassword(const QString &password, const QByteArray &msg, bool *ok)
{
    QByteArray result=gCrypt::decode(password,msg);

    if (result.isEmpty())
        return result;

    return result.right(result.length()/gCrypt::repeated);
}

QString gCrypt::getCryptLibraryVersion()
{
    return QString("Botan v%1.%2.%3").arg(BOTAN_VERSION_MAJOR).arg(BOTAN_VERSION_MINOR).arg(BOTAN_VERSION_PATCH);
}

QByteArray gCrypt::encode(const QString &password,const QByteArray &msg,bool *ok)
{
    if (ok)
        *ok=true;

    try {
        Botan::LibraryInitializer initBotan("thread_safe=true");

        QByteArray hash=QCryptographicHash::hash(password.toUtf8(),QCryptographicHash::Md5);

        unsigned char *cKey=(unsigned char *)hash.data();
        Botan::SymmetricKey key(cKey,hash.length());
        Botan::InitializationVector iv(cKey,hash.length());
        Botan::Pipe pipe(Botan::get_cipher("AES-128/CBC/CTS", key, iv, Botan::ENCRYPTION));

        unsigned char *cMsg=(unsigned char *)msg.data();
        pipe.process_msg(cMsg,msg.length());
        char *res=new char[msg.length()];
        pipe.read((unsigned char *)res,msg.length());

        QByteArray result=QByteArray().fromRawData((char *)res,msg.length());

        return result;
    }catch(std::exception &e){
        if (ok)
            *ok=false;
    }

    return QByteArray();
}

QByteArray gCrypt::decode(const QString &password, const QByteArray &msg, bool *ok)
{
    if (ok)
        *ok=true;

    try {
        Botan::LibraryInitializer initBotan("thread_safe=true");

        QByteArray hash=QCryptographicHash::hash(password.toUtf8(),QCryptographicHash::Md5);

        unsigned char *cKey=(unsigned char *)hash.data();
        Botan::SymmetricKey key(cKey,hash.length());
        Botan::InitializationVector iv(cKey,hash.length());

        Botan::Pipe pipe(Botan::get_cipher("AES-128/CBC/CTS", key, iv, Botan::DECRYPTION));

        unsigned char *cMsg=(unsigned char *)msg.data();
        pipe.process_msg(cMsg,msg.length());
        unsigned char *res=new unsigned char[msg.length()];
        pipe.read(res,msg.length());

        return QByteArray().fromRawData((char *)res,msg.length());
    }catch(std::exception &e){
        if (ok)
            *ok=false;
    }

    return QByteArray();
}
