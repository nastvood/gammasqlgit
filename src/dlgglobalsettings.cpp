﻿#include "dlgglobalsettings.h"
#include "ui_dlgglobalsettings.h"

DlgGlobalSettings::DlgGlobalSettings(const QString &organization,const QString &application,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgGlobalSettings)
{
    ui->setupUi(this);

    settings=new QSettings(organization,application,this);

    tkInterpPath=settings->value("tkInterpPath","C:/Program Files/Git/bin/wish.exe").toString();
    gitGuiPath=settings->value("gitGuiPath","C:/Program Files/Git/libexec/git-core/git-gui").toString();
}

DlgGlobalSettings::~DlgGlobalSettings()
{
    delete ui;
}

void DlgGlobalSettings::showEvent(QShowEvent *)
{
    ui->leTkInterpPath->setText(tkInterpPath);
    ui->leGiGuiPath->setText(gitGuiPath);
}

QString DlgGlobalSettings::getTkInterpPath()
{
    return tkInterpPath;
}

QString DlgGlobalSettings::getGitGuiPath()
{
    return gitGuiPath;
}

void DlgGlobalSettings::setTkInterpPath(const QString &value)
{
    tkInterpPath=value;
    settings->setValue("tkInterpPath",tkInterpPath);
}

void DlgGlobalSettings::setGitGuiPath(const QString &value)
{
    gitGuiPath=value;
    settings->setValue("gitGuiPath",gitGuiPath);
}

void DlgGlobalSettings::on_pbClose_clicked()
{
    if ((tkInterpPath!=ui->leTkInterpPath->text()) || (gitGuiPath!=ui->leGiGuiPath->text())){
        int ret=QMessageBox::question(this,
                              settings->applicationName(),
                              "Сохранить изменения",
                              QMessageBox::Yes,
                              QMessageBox::No);

        if (ret==QMessageBox::Yes){
            on_pbSave_clicked();
            return;
        }
    }

    close();
}

void DlgGlobalSettings::on_pbSave_clicked()
{
    if (tkInterpPath!=ui->leTkInterpPath->text())
        setTkInterpPath(ui->leTkInterpPath->text());

    if (gitGuiPath!=ui->leGiGuiPath->text())
        setGitGuiPath(ui->leGiGuiPath->text());

    close();
}

void DlgGlobalSettings::on_pbTkInterpPath_clicked()
{
    QString fileName=QFileDialog::getOpenFileName(this,"Tcl/Tk интерпретатор",tkInterpPath,"wish.exe");
    if (!fileName.isEmpty())
        ui->leTkInterpPath->setText(fileName);
}

void DlgGlobalSettings::on_pbGiGuiPath_clicked()
{
    QString fileName=QFileDialog::getOpenFileName(this,"git-gui скрипт",gitGuiPath,"git-gui");
    if (!fileName.isEmpty())
        ui->leGiGuiPath->setText(fileName);
}

