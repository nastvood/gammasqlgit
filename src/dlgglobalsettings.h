#ifndef DLGGLOBALSETTINGS_H
#define DLGGLOBALSETTINGS_H

#include <QDialog>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

namespace Ui {
class DlgGlobalSettings;
}

class DlgGlobalSettings : public QDialog
{
    Q_OBJECT
    
    public:
        explicit DlgGlobalSettings(const QString &organization,const QString &application,QWidget *parent = 0);
        ~DlgGlobalSettings();
        QString getTkInterpPath();
        QString getGitGuiPath();
        void setTkInterpPath(const QString &value);
        void setGitGuiPath(const QString &value);

    private slots:
        void on_pbClose_clicked();
        void on_pbSave_clicked();
        void on_pbTkInterpPath_clicked();
        void on_pbGiGuiPath_clicked();

    private:
        Ui::DlgGlobalSettings *ui;

        QSettings *settings;
        QString tkInterpPath;
        QString gitGuiPath;

        void showEvent(QShowEvent *);
};

#endif // DLGGLOBALSETTINGS_H
