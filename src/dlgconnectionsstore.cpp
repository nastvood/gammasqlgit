#include "dlgconnectionsstore.h"
#include "ui_dlgconnectionsstore.h"

DlgConnectionsStore::DlgConnectionsStore(const QString &nameProject_,const QString &pathProject_,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgConnectionsStore)
{
    ui->setupUi(this);

    nameProject=nameProject_;
    pathProject=pathProject_;
}

DlgConnectionsStore::~DlgConnectionsStore()
{
    if (db.isOpen())
        db.close();

    delete ui;
}

void DlgConnectionsStore::on_pbClose_clicked()
{
    close();
}

void DlgConnectionsStore::init(const QString &password_)
{
    projectPass=password_;

    QStringList tables;
    tables<<SQL_TABLE_CONNECTIONS<<SQL_TABLE_CONNECTIONS_PASSWORD;
    QStringList indexList;
    indexList<<SQL_INDEX_TABLE_CONNECTIONS;

    db=QSqlDatabase::addDatabase("QSQLITE",gHelper::createDbConnectionName());
    db.setDatabaseName(QDir::toNativeSeparators(pathProject+QDir::separator()+nameProject+QDir::separator()+nameProject+".db"));
    modelConnections=new QSqlTableModel(this,db);

    if (db.open()){                
        QStringList dbTables=db.tables();
        for (int i=0;i<tables.count();i++){
            if (dbTables.indexOf(tables.at(i))<0){
                if (tables.at(i)==SQL_TABLE_CONNECTIONS)
                    db.exec(SQL_SCRIPT_CREATE_TABLE_CONNECTIONS);
                else if (tables.at(i)==SQL_TABLE_CONNECTIONS_PASSWORD){
                    db.exec(SQL_SCRIPT_CREATE_TABLE_CONNECTIONS_PASSWORD);
                    db.exec(SQL_SCRIPT_INSERT_TABLE_CONNECTIONS_PASSWORD);
                }
            }
        }

        QStringList dbIndexes=indexes(SQL_TABLE_CONNECTIONS);
        for (int i=0;i<indexList.count();i++){
            if (dbIndexes.indexOf(indexList.at(i))<0)
                if (indexList.at(i)==SQL_INDEX_TABLE_CONNECTIONS)
                    db.exec(SQL_SCRIPT_CRAETE_INDEX_ON_TABLE_CONNECTIONS);
        }

        modelConnections->setTable("tbl_connections");
        modelConnections->setHeaderData(1,Qt::Horizontal,"Хост");
        modelConnections->setHeaderData(2,Qt::Horizontal,"Порт");
        modelConnections->setHeaderData(3,Qt::Horizontal,"БД");
        modelConnections->setHeaderData(4,Qt::Horizontal,"Пользователь");
        modelConnections->select();

        ui->tvConnections->setModel(modelConnections);
        ui->tvConnections->hideColumn(0);
        ui->tvConnections->hideColumn(5);
        ui->tvConnections->hideColumn(6);
        ui->tvConnections->hideColumn(7);
        ui->tvConnections->show();
    }
}

bool DlgConnectionsStore::changePasswordCryptKey(const QString &oldPassword,const QString &newPassword)
{
    if (!db.isOpen())
        if(!db.open())
            return false;

    if (!checkPasswordCryptKey(oldPassword))
        return false;

    QSqlQuery query("SELECT id_conn,passwd FROM tbl_connections",db);
    while (query.next()) {
        qlonglong id = query.value(0).toLongLong();
        QByteArray connectionPassword = gCrypt::decodePassword(oldPassword,query.value(1).toByteArray());

        QSqlQuery queryUpdConn(db);
        queryUpdConn.prepare("UPDATE tbl_connections SET passwd=:passwd WHERE id_conn=:id");
        queryUpdConn.bindValue(":passwd",gCrypt::encodePassword(newPassword,connectionPassword));
        queryUpdConn.bindValue(":id",id);
        if (!queryUpdConn.exec())
            return false;
    }

    bool changed=setPasswordCryptKey(newPassword);
    if (!changed)
        return false;

    projectPass=newPassword;
    return true;
}

bool DlgConnectionsStore::setPasswordCryptKey(const QString &password)
{
    if (!db.isOpen())
        if(!db.open())
            return false;

    QSqlQuery query(db);
    query.prepare("UPDATE tbl_conn_password SET password=:password");
    query.bindValue(":password",gCrypt::encodePassword(password,password.toUtf8()));
    return query.exec();
}

bool DlgConnectionsStore::checkPasswordCryptKey(const QString &password)
{
    if (!db.isOpen())
        if(!db.open())
            return false;

    QByteArray encodeConnectionPassword;
    QSqlQuery query("SELECT password FROM tbl_conn_password",db);
    if (query.next()){
        encodeConnectionPassword=query.value(0).toByteArray();
        if (encodeConnectionPassword.isEmpty())
            return false;
    }else{
        return false;
    }

    QString connectionPassword=gCrypt::decodePassword(password,encodeConnectionPassword);

    if (connectionPassword==password)
        return true;

    return false;
}

gDescriptionConnections DlgConnectionsStore::getDescriptionConnections() const
{
    gDescriptionConnections connections;

    for (int i=0;i<modelConnections->rowCount();i++){
        //qlonglong id=modelConnections->data(modelConnections->index(i,0)).toLongLong();
        QString host=modelConnections->data(modelConnections->index(i,1)).toString();
        unsigned short port=(unsigned short)modelConnections->data(modelConnections->index(i,2)).toUInt();
        QString dbName=modelConnections->data(modelConnections->index(i,3)).toString();
        QString user=modelConnections->data(modelConnections->index(i,4)).toString();
        QString passwd=gCrypt::decodePassword(projectPass, modelConnections->data(modelConnections->index(i,5)).toByteArray());
        QString textCodec=modelConnections->data(modelConnections->index(i,6)).toString();
        QString dbType=modelConnections->data(modelConnections->index(i,7)).toString();

        connections.append(gDescriptionConnection(host,port,dbName,user,passwd,textCodec,dbType));
    }

    return connections;
}

QStringList DlgConnectionsStore::indexes(const QString &tableName)
{
    QStringList indexList;

    if (!db.isOpen())
        if (!db.open())
            return indexList;

    QSqlQuery query("SELECT name FROM sqlite_master WHERE type='index' AND tbl_name='"+tableName+"'",db);
    while(query.next()){
        indexList.append(query.value(0).toString());
    }

    return indexList;
}

void DlgConnectionsStore::showEvent(QShowEvent *)
{
    if (modelConnections->rowCount()>0)
        ui->tvConnections->selectRow(0);
}

void DlgConnectionsStore::on_pbAddConn_clicked()
{
    gDescriptionConnection connection=DlgSQLConnection::getConnection();
    if (!connection.empty){
        QSqlQuery query(db);
        QString sql="INSERT INTO tbl_connections(host,port,db,user,passwd,text_codec,db_type) VALUES(:host,:port,:db,:user,:passwd,:text_codec,:db_type)";
        query.prepare(sql);
        query.bindValue(":host",connection.host);
        query.bindValue(":port",connection.port);
        query.bindValue(":db",connection.dbName);
        query.bindValue(":user",connection.user);
        query.bindValue(":passwd",gCrypt::encodePassword(projectPass,connection.passwd.toUtf8()));
        query.bindValue(":text_codec",connection.textCodec);
        query.bindValue(":db_type",connection.dbType);
        if (query.exec()){
            modelConnections->select();

            emit sigNewConnection(connection.host,connection.dbName);
        }
    }
}

void DlgConnectionsStore::on_pbEdit_clicked()
{
    QModelIndexList selectedRows = ui->tvConnections->selectionModel()->selectedRows();

    if (selectedRows.count()>0){
        int row=selectedRows.at(0).row();

        qlonglong id=modelConnections->data(modelConnections->index(row,0)).toLongLong();
        QString host=modelConnections->data(modelConnections->index(row,1)).toString();
        unsigned short port=(unsigned short)modelConnections->data(modelConnections->index(row,2)).toUInt();
        QString dbName=modelConnections->data(modelConnections->index(row,3)).toString();
        QString user=modelConnections->data(modelConnections->index(row,4)).toString();
        QString passwd=gCrypt::decodePassword(projectPass, modelConnections->data(modelConnections->index(row,5)).toByteArray());
        QString textCodec=modelConnections->data(modelConnections->index(row,6)).toString();
        QString dbType=modelConnections->data(modelConnections->index(row,7)).toString();

        QString hostOld=host;
        QString dbNameOld=dbName;

        gDescriptionConnection connection(host,port,dbName,user,passwd,textCodec,dbType);
        bool saved=DlgSQLConnection::getConnection(connection);
        if (saved){
            QSqlQuery query(db);
            QString sql="UPDATE tbl_connections SET host=:host,port=:port,db=:db,user=:user,passwd=:passwd,text_codec=:text_codec,db_type=:db_type WHERE id_conn=:id";
            query.prepare(sql);
            query.bindValue(":id",id);
            query.bindValue(":host",connection.host);
            query.bindValue(":port",connection.port);
            query.bindValue(":db",connection.dbName);
            query.bindValue(":user",connection.user);
            query.bindValue(":passwd",gCrypt::encodePassword(projectPass,connection.passwd.toUtf8()));
            query.bindValue(":text_codec",connection.textCodec);
            query.bindValue(":db_type",connection.dbType);
            if (query.exec()){
                modelConnections->select();

                emit sigUpdateConnection(connection.host,connection.dbName,hostOld,dbNameOld);
            }
        }
    }else{
        QMessageBox::information(this,PROG_NAME,"Выберите соединение");
    }
}

void DlgConnectionsStore::on_pbRemove_clicked()
{
    QModelIndexList selectedRows = ui->tvConnections->selectionModel()->selectedRows();

    if (selectedRows.count()>0){
        int answer=QMessageBox::question(this,PROG_NAME,"Вы действительно желаете удалить соединение?\nБудут удалены все скрипты откносящиеся к нему",QMessageBox::Yes,QMessageBox::No);
        if (answer==QMessageBox::No)
            return;

        int row=selectedRows.at(0).row();

        qlonglong id=modelConnections->data(modelConnections->index(row,0)).toLongLong();
        QString host=modelConnections->data(modelConnections->index(row,1)).toString();
        QString dbName=modelConnections->data(modelConnections->index(row,3)).toString();
        emit sigRemoveConnection(host,dbName);
        QSqlQuery query(db);
        QString sql="DELETE FROM tbl_connections WHERE id_conn=:id";
        query.prepare(sql);
        query.bindValue(":id",id);
        if (query.exec()){
            modelConnections->select();

            emit sigRemoveConnection(host,dbName);
        }
    }else{
        QMessageBox::information(this,PROG_NAME,"Выберите соединение");
    }
}

void DlgConnectionsStore::on_tvConnections_doubleClicked(const QModelIndex &)
{
    on_pbEdit_clicked();
}
