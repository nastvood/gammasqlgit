#ifndef GCODEEDIT_H
#define GCODEEDIT_H

#include <QTextEdit>
#include <QPainter>
#include <QPaintEvent>
#include <QTextBlock>
#include <QPlainTextEdit>

class gLineNumberArea;

class gCodeEdit : public QPlainTextEdit
{
        Q_OBJECT

    public:
        explicit gCodeEdit(QWidget *parent = 0);
        void lineNumberAreaPaintEvent(QPaintEvent *e);
        int lineNumberAreaWidth();

    signals:

    private slots:
        void updateLineNumberAreaWidth(int);
        void highlightCurrentLine();
        void updateLineNumberArea(const QRect &rect, int dy);

    protected:
        void resizeEvent(QResizeEvent *event);

    private:
        QWidget *lineNumberArea;

};

class gLineNumberArea : public QWidget
{
    public:
        gLineNumberArea(gCodeEdit *editor) : QWidget(editor) {
            codeEditor = editor;
        }

        QSize sizeHint() const {
            return QSize(codeEditor->lineNumberAreaWidth(), 0);
        }

    protected:
        void paintEvent(QPaintEvent *event) {
            codeEditor->lineNumberAreaPaintEvent(event);
        }

    private:
        gCodeEdit *codeEditor;
};




#endif // QCODEEDIT_H
