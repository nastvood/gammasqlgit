#ifndef GGIT_H
#define GGIT_H

#include <QObject>
#include <QDir>
#include <QDebug>
#include <QProcess>
#include <QStringList>

#include <git2.h>
#include <memory>

struct BranchDescr {
    QString name;
    git_branch_t type;
};

typedef QVector<BranchDescr> BranchDescrList;

class gGit : public QObject
{
    Q_OBJECT
    public:
        explicit gGit(QObject *parent = 0);
        ~gGit();
        void init(const QString path_);
        QString getVersion();
        static QString getLibGit2Version();
        QString getDiff(const QString &file);
        QString getStatus(const QString &file);
        QString getCurrentBranch();
        BranchDescrList getListOfBranches();
        void setBranche(const QString &branche, git_branch_t type);
        void repositoryReopen();

    signals:

    public slots:

    private:
        QString path;
        QString currentBranch;
        QString runCommand(const QString &command);

        git_repository *repo;
        git_reference *ref;

        static int callbackDiffData(const git_diff_delta *delta, const git_diff_hunk *hunk,
                                    const git_diff_line *line,
                                    void *payload);
        static int callbackReferenceName(const char *name, void *payload);
};

#endif // GGIT_H
