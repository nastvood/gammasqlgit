#include "gsqlhighlighter.h"

gSQLHighlighter::gSQLHighlighter(QTextDocument *parent)
    :QSyntaxHighlighter(parent)
{
    iniKeyWords();
}

void gSQLHighlighter::highlightBlock(const QString &text){
    enum {NORMAL_STATE=-1,COMMENT_STATE,TEXT_STATE};

    int state=previousBlockState();
    int start = 0;
    QString word;

    for (int i = 0; i < text.length(); ++i) {
        if (text.mid(i, 2) == "*/") {
            state = NORMAL_STATE;
            setFormat(start, i - start + 2, Qt::darkGreen);
        }else{
            if (text.mid(i, 2) == "--") {
                setFormat(i, text.length() - i, Qt::darkGreen);
                break;
            } else if (text.mid(i, 2) == "/*") {
                start = i;
                state = COMMENT_STATE;
            }
        }

        if(state==TEXT_STATE && text.at(i)=='\''){
            state = NORMAL_STATE;
            setFormat(start, i - start+1, Qt::darkYellow);
            continue;
        }

        if (state==NORMAL_STATE){
            if (text.at(i)=='\''){
                state=TEXT_STATE;
                start=i;
                continue;
            }

            if (!(text.at(i).isSpace() || text.at(i)==',' || text.at(i)=='(' || text.at(i)==')')){
                word+=text.at(i);
            }else{                
                setFormatKeysWords(word,i);
                word="";
            }
        }
    }

    if (!word.isEmpty())
        setFormatKeysWords(word,text.length());

    if (state == TEXT_STATE)
        setFormat(start, text.length() - start, Qt::darkYellow);

    if (state == COMMENT_STATE)
        setFormat(start, text.length() - start, Qt::darkGreen);

    setCurrentBlockState(state);
};

void gSQLHighlighter::setFormatKeysWords(const QString &word,int pos)
{
    if (keyWords.indexOf(word.trimmed().toUpper())>-1){
        setFormat(pos-word.length(),word.length(), Qt::blue);
    }else{
        if (word.trimmed().mid(0,1)=="@")
            setFormat(pos-word.length(),word.length(), Qt::darkCyan);
        if (word.trimmed().mid(0,1)=="#")
            setFormat(pos-word.length(),word.length(), Qt::darkGray);
    }
}


void gSQLHighlighter::iniKeyWords()
{
    keyWords<<"ADD"<<"EXCEPT"<<"PERCENT"<<"ALL"<<"EXEC"<<"PLAN"<<"ALTER"<<"EXECUTE"<<"PRECISION"
            <<"AND"<<"EXISTS"<<"PRIMARY"<<"ANY"<<"EXIT"<<"PRINT"<<"AS"<<"FETCH"<<"PROC"<<"ASC"<<"FILE"<<"PROCEDURE"
            <<"AUTHORIZATION"<<"FILLFACTOR"<<"PUBLIC"<<"BACKUP"<<"FOR"<<"RAISERROR"<<"BEGIN"<<"FOREIGN"<<"READ"
            <<"BETWEEN"<<"FREETEXT"<<"READTEXT"<<"BREAK"<<"FREETEXTTABLE"<<"RECONFIGURE"
            <<"BROWSE"<<"FROM"<<"REFERENCES"<<"BULK"<<"FULL"<<"REPLICATION"
            <<"BY"<<"FUNCTION"<<"RESTORE"<<"CASCADE"<<"GOTO"<<"RESTRICT"
            <<"CASE"<<"GRANT"<<"RETURN"<<"CHECK"<<"GROUP"<<"REVOKE"
            <<"CHECKPOINT"<<"HAVING"<<"RIGHT"<<"CLOSE"<<"HOLDLOCK"<<"ROLLBACK"
            <<"CLUSTERED"<<"IDENTITY"<<"ROWCOUNT"<<"COALESCE"<<"IDENTITY_INSERT"<<"ROWGUIDCOL"
            <<"COLLATE"<<"IDENTITYCOL"<<"RULE"<<"COLUMN"<<"IF"<<"SAVE"
            <<"COMMIT"<<"IN"<<"SCHEMA"<<"COMPUTE"<<"INDEX"<<"SELECT"
            <<"CONSTRAINT"<<"INNER"<<"SESSION_USER"<<"CONTAINS"<<"INSERT"<<"SET"
            <<"CONTAINSTABLE"<<"INTERSECT"<<"SETUSER"<<"CONTINUE"<<"INTO"<<"SHUTDOWN"
            <<"CONVERT"<<"IS"<<"SOME"<<"CREATE"<<"JOIN"<<"STATISTICS"
            <<"CROSS"<<"KEY"<<"SYSTEM_USER"<<"CURRENT"<<"KILL"<<"TABLE"
            <<"CURRENT_DATE"<<"LEFT"<<"TEXTSIZE"<<"CURRENT_TIME"<<"LIKE"<<"THEN"
            <<"CURRENT_TIMESTAMP"<<"LINENO"<<"TO"<<"CURRENT_USER"<<"LOAD"<<"TOP"
            <<"CURSOR"<<"NATIONAL"<<"TRAN"<<"DATABASE"<<"NOCHECK"<<"TRANSACTION"
            <<"DBCC"<<"NONCLUSTERED"<<"TRIGGER"<<"DEALLOCATE"<<"NOT"<<"TRUNCATE"
            <<"DECLARE"<<"NULL"<<"TSEQUAL"<<"DEFAULT"<<"NULLIF"<<"UNION"
            <<"DELETE"<<"OF"<<"UNIQUE"<<"DENY"<<"OFF"<<"UPDATE"
            <<"DESC"<<"OFFSETS"<<"UPDATETEXT"<<"DISK"<<"ON"<<"USE"
            <<"DISTINCT"<<"OPEN"<<"USER"<<"DISTRIBUTED"<<"OPENDATASOURCE"<<"VALUES"
            <<"DOUBLE"<<"OPENQUERY"<<"VARYING"<<"DROP"<<"OPENROWSET"<<"VIEW"
            <<"DUMMY"<<"OPENXML"<<"WAITFOR"<<"DUMP"<<"OPTION"<<"WHEN"
            <<"ELSE"<<"OR"<<"WHERE"<<"END"<<"ORDER"<<"WHILE"
           <<"ERRLVL"<<"OUTER"<<"WITH"<<"ESCAPE"<<"OVER"<<"WRITETEXT";
}


