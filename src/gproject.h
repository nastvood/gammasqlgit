#ifndef GPROJECT_H
#define GPROJECT_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QDebug>
#include <QInputDialog>
#include <QProcess>
#include <QSyntaxHighlighter>
#include <QDesktopServices>
#include <QUrl>
#include <QSignalMapper>
#include <QComboBox>
#include <QLabel>
#include <QMessageBox>
#include <QMetaEnum>

#include "gprojectcore.h"
#include "dlgglobalsettings.h"
#include "dlgselobj.h"
#include "gsettings.h"
#include "gsqlhighlighter.h"
#include "ggitdiffhighlighter.h"
#include "gcodeedit.h"
#include "gversion.h"
#include "dlgconnectionsstore.h"

#define PROJECT_ITEM_ROLE_TYPE Qt::UserRole+1
#define PROJECT_ITEM_ROLE_HOST Qt::UserRole+2
#define PROJECT_ITEM_ROLE_DB Qt::UserRole+3
#define PROJECT_ITEM_ROLE_OBJECT_TYPE Qt::UserRole+4
#define PROJECT_ITEM_ROLE_DB_TYPE Qt::UserRole+5

#define BRANCH_ROLE_NAME Qt::UserRole+6
#define BRANCH_ROLE_TYPE Qt::UserRole+7

namespace Ui {
    class gProject;
}

class gProject : public QMainWindow
{
    Q_OBJECT
    
    public:
        explicit gProject(const QString &nameProject,
                          const QString &pathProject,
                          DlgGlobalSettings *const globalSettings_,
                          QWidget *parent = 0);
        ~gProject();
        void release();

        enum gProjectTreeItemType {GPTIT_UNKNOW=0,GPTIT_HOST=1,GPTIT_DB=2,GPTIT_OBJECTS=3,GPTIT_SCRIPT=4};

        bool openProject(const QString &password, bool create=true);
        void openProjectDebug();
        QString getName();
        QString getPath();
        static bool isValidProject(const QString &projectPath);

    public slots:
        void showSettings();
        void showSPUpdate();
        void showFUpdate();
        void showTUpdate();
        void showVUpdate();
        void showGitGui();
        void updateAll();
        void updateObjectsInDialog(gObjectDBType objType);

    private slots:
        void closeProject();
        void on_treeProject_customContextMenuRequested(const QPoint &pos);
        void cntMnExplorer();
        void cntMnUpdate();
        void cntMnRemove();
        void slotRemoveObject(const QString &host, const QString &dbName, const QString &objectName, gObjectDBType objType);
        void slotAddObject(const QString &host, const QString &dbName, const QString &objectName, gObjectDBType objType);
        void slotNewConnection(const QString &, const QString &);
        void slotUpdateConnection(const QString &host, const QString &, const QString &hostOld, const QString &);
        void slotRemoveConnection(const QString &host, const QString &dbName);
        void showConnectionsStore();
        void on_treeProject_itemSelectionChanged();
        void changeBranche(const QString &branche);
        void finishedGitGui(int);
        void on_treeProject_doubleClicked(const QModelIndex &);
        void messActionText(const QString &mess);
        void slotChangePasswordProject(const QString &oldPassword,const QString &newPassword);
        void slotUpdateObject(const QString &host, const QString &dbName,gObjectDBType objectType, const QString &objectName,gObjectUpdateType updateType);

    signals:
        void sigMessActionText(const QString &mess);

    private:
        Ui::gProject *ui;
        gProjectCore *core;
        DlgSelObj *dlgSelObj;
        QString name;
        QString path;
        gSettings *settings;
        gSQLHighlighter *sqlHighlighter;
        gGitDiffHighlighter *gitDiffHighlighter;
        QProcess processGitGui;
        DlgGlobalSettings *globalSettings;
        DlgConnectionsStore *connectionsStore;

        QComboBox *cmbBranches;
        QLabel *lblBranches;

        void buildProjectTree();
        void buildProjectTree(const QString &path_,QTreeWidgetItem *item);
        QString getPathByItem(QTreeWidgetItem *item_);
        void selTreeItem(QTreeWidgetItem *item);
        QTreeWidgetItem *findItem(const QString &text, gProjectTreeItemType itemType, QTreeWidgetItem *parent=NULL);
        QTreeWidgetItem * findItemObjects(const QString &host, const QString &dbName, gObjectDBType objType);
        QTreeWidgetItem * findItemScript(const QString &host, const QString &dbName, gObjectDBType objectType, const QString &objectName);

        void removeObjectsDir(const QString &host, const QString &dbName, gObjectDBType objType);
        void removeDbDir(const QString &host, const QString &dbName);
        void removeHostDir(const QString &host);

        void createTreeContextMenu();
        void createToolBarMenu();
        void updateCmbBranches();

        QAction *actGit;
        QAction *actSettings;
        QAction *actClose;
        QAction *actSPUpdate;
        QAction *actFUpdate;
        QAction *actTUpdate;
        QAction *actVUpdate;
        QAction *actUpdateAll;
        QAction *actSqlConnections;

        QMenu *contextMenuFileTree;
        QAction *actCntMnExplorer;
        QAction *actCntMnUpdate;
        QAction *actCntMnRemove;

        QSignalMapper *mapper;
};

#endif // GPROJECT_H
