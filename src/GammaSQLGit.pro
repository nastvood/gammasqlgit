#-------------------------------------------------
#
# Project created by QtCreator 2013-04-23T15:57:12
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_LFLAGS = -static -static-libgcc

TARGET = GammaSQLGit
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp\
        mainwindow.cpp \
    ggit.cpp \
    gproject.cpp \
    gprojectcore.cpp \
    gdb.cpp \
    dlgselobj.cpp \
    dlgcreateproject.cpp \
    gsettings.cpp \
    gsqlhighlighter.cpp \
    gcodeedit.cpp \
    ggitdiffhighlighter.cpp \
    dlgglobalsettings.cpp \
    dlgsqlconnection.cpp \
    gdescriptionconntion.cpp \
    gcrypt.cpp \
    dlgconnectionsstore.cpp

HEADERS  += mainwindow.h \
    ggit.h \
    gproject.h \
    gprojectcore.h \
    gdb.h \
    dlgselobj.h \
    gsqlobj.h \
    dlgcreateproject.h \
    gsettings.h \
    gsqlhighlighter.h \
    gversion.h \
    gcodeedit.h \
    ggitdiffhighlighter.h \
    dlgglobalsettings.h \
    gsqlsettingsscripts.h \
    dlgsqlconnection.h \
    gdescriptionconntion.h \
    gcrypt.h \
    dlgconnectionsstore.h \
    ghelper.h

FORMS    += mainwindow.ui \
    gproject.ui \
    dlgselobj.ui \
    dlgcreateproject.ui \
    gsettings.ui \
    dlgglobalsettings.ui \
    dlgsqlconnection.ui \
    dlgconnectionsstore.ui

LIBS += -lbotan -lpthread -lgit2 -leay32 -lssleay32

RESOURCES += \
    res.qrc

OTHER_FILES += \
    win.rc

win32{
    RC_FILE += win.rc
}
