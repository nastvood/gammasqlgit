#ifndef GSETTINGS_H
#define GSETTINGS_H

#include <QDialog>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QInputDialog>
#include <QCryptographicHash>
#include <QDateTime>
#include <QMessageBox>

#include <QCryptographicHash>
#include <botan/botan.h>

#include "gsqlsettingsscripts.h"
#include "dlgsqlconnection.h"
#include "gcrypt.h"
#include "gversion.h"

namespace Ui {
class gSettings;
}
    class gSettings : public QDialog
    {
        Q_OBJECT

    public:
        explicit gSettings(const QString &nameProject_, const QString &pathProject_, QWidget *parent = 0);
        ~gSettings();
        QString getProjectPassword() const;
        void setProjectPass(const QString &value);
        void initSettings();

    signals:
        void sigChangeProjectPassword(const QString &oldPassword,const QString &newPassword);

    protected:
        void showEvent(QShowEvent *);

    private slots:
        void on_pbCancel_clicked();
        void on_pbSave_clicked();

    private:
        Ui::gSettings *ui;
        QString projectPass;
        QString nameProject;
        QString pathProject;
        QString echoPassValue;
};

#endif // GSETTINGS_H
