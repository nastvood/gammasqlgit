#include "dlgsqlconnection.h"
#include "ui_dlgsqlconnection.h"

DlgSQLConnection::DlgSQLConnection(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgSQLConnection)
{
    ui->setupUi(this);
}

DlgSQLConnection::~DlgSQLConnection()
{
    delete ui;
}

gDescriptionConnection DlgSQLConnection::getConnection()
{
    DlgSQLConnection dlgSQLConnection;

    gDescriptionConnection connection;
    if (dlgSQLConnection.exec()==QDialog::Accepted){
        connection=dlgSQLConnection.getValues();
    }

    return connection;
}

bool DlgSQLConnection::getConnection(gDescriptionConnection &connection)
{
    DlgSQLConnection dlgSQLConnection;

    dlgSQLConnection.ui->leSQLHost->setText(connection.host);
    dlgSQLConnection.ui->sbSqlPort->setValue(connection.port);
    dlgSQLConnection.ui->leSQLDBName->setText(connection.dbName);
    dlgSQLConnection.ui->leSQLUser->setText(connection.user);
    dlgSQLConnection.ui->leSQLPasswd->setText(connection.passwd);
    dlgSQLConnection.ui->cmbTextCodec->setItemText(0,connection.textCodec);
    dlgSQLConnection.ui->cmbDbType->setItemText(0,connection.dbType);

    if (dlgSQLConnection.exec()==QDialog::Accepted){
        connection.host=dlgSQLConnection.ui->leSQLHost->text();
        connection.port=dlgSQLConnection.ui->sbSqlPort->value();
        connection.dbName=dlgSQLConnection.ui->leSQLDBName->text();
        connection.user=dlgSQLConnection.ui->leSQLUser->text();
        connection.passwd=dlgSQLConnection.ui->leSQLPasswd->text();
        connection.textCodec=dlgSQLConnection.ui->cmbTextCodec->currentText();
        connection.dbType=dlgSQLConnection.ui->cmbDbType->currentText();

        return true;
    }

    return false;
}

void DlgSQLConnection::on_pbClose_clicked()
{
    reject();
}

gDescriptionConnection DlgSQLConnection::getValues()
{
    return gDescriptionConnection(ui->leSQLHost->text(),
                                ui->sbSqlPort->value(),
                                ui->leSQLDBName->text(),
                                ui->leSQLUser->text(),
                                ui->leSQLPasswd->text(),
                                ui->cmbTextCodec->currentText(),
                                ui->cmbDbType->currentText());
}

void DlgSQLConnection::on_pbSave_clicked()
{
    accept();
}

void DlgSQLConnection::on_pbTest_clicked()
{
    QSqlDatabase db=QSqlDatabase::addDatabase(ui->cmbDbType->currentText(),gHelper::createDbConnectionName());
    db.setDatabaseName(gDB::createConnectionString(ui->leSQLHost->text(),
                                                   ui->leSQLDBName->text(),
                                                   ui->leSQLUser->text(),
                                                   ui->leSQLPasswd->text()));
    if (db.open()){
        QMessageBox::information(this,PROG_NAME,"Успешное соединение");
        db.close();
    }else{
        QMessageBox::warning(this,PROG_NAME,"Ошибка: "+db.lastError().text());
    }
}
