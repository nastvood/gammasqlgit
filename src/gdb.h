#ifndef GDB_H
#define GDB_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QDateTime>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QFile>

#include <windows.h>
#include <sql.h>
#include <sqlext.h>

#include "gsqlobj.h"
#include "ghelper.h"

enum gObjectDBType {GDB_NONE=-1,GDB_P=1,GDB_F=2,GDB_T=3,GDB_V=4};

class gDB : public QObject
{
    Q_OBJECT

    public:
        explicit gDB(QObject *parent = 0);
        ~gDB();

        void setDatabaseName(const QString &host, const QString &dbName, const QString &user, const QString &pass);
        void getListOfSQLObjcts(gListSQlObjts &objects, gObjectDBType objType);
        QByteArray getObjText(const QString &objName_, gObjectDBType objType);
        bool isOpen();
        static QVector<gObjectDBType> gObjectDBTypeList;
        static QString createConnectionString(const QString &host, const QString &dbName, const QString &user, const QString &pass);

    signals:

    public slots:

    private:
        QSqlDatabase db;
        QString getTypeObjChars(gObjectDBType objType);
        QByteArray getTriggerOfTable(const QString &table,const QString &schema);
        QString createTableScript;
        QString createTriggersScript;
    
};

#endif // GDB_H
