#ifndef DLGCONNECTIONSSTORE_H
#define DLGCONNECTIONSSTORE_H

#include <QDialog>

#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QDir>
#include <QDebug>
#include <QMessageBox>

#include "gsqlsettingsscripts.h"
#include "gdescriptionconntion.h"
#include "gcrypt.h"
#include "dlgsqlconnection.h"
#include "gversion.h"
#include "ghelper.h"

namespace Ui {
class DlgConnectionsStore;
}

class DlgConnectionsStore : public QDialog
{
    Q_OBJECT
    
    public:
        explicit DlgConnectionsStore(const QString &nameProject_,const QString &pathProject_,QWidget *parent = 0);
        ~DlgConnectionsStore();
        gDescriptionConnections getDescriptionConnections() const;

        void init(const QString &password_);
        bool changePasswordCryptKey(const QString &oldPassword, const QString &newPassword);
        bool setPasswordCryptKey(const QString &password);
        bool checkPasswordCryptKey(const QString &password);

    private slots:
        void on_pbClose_clicked();
        void on_pbAddConn_clicked();
        void on_pbEdit_clicked();
        void on_pbRemove_clicked();
        void on_tvConnections_doubleClicked(const QModelIndex &index);

    signals:
        void sigNewConnection(const QString &host,const QString &dbName);
        void sigUpdateConnection(const QString &host,const QString &dbName,const QString &hostOld,const QString &dbNameOld);
        void sigRemoveConnection(const QString &host,const QString &dbName);

    private:
        Ui::DlgConnectionsStore *ui;
        QString projectPass;
        QString nameProject;
        QString pathProject;
        QSqlDatabase db;
        QSqlTableModel *modelConnections;
        QString echoPassValue;

        QStringList indexes(const QString &tableName);
        void showEvent(QShowEvent *);
};

#endif // DLGCONNECTIONSSTORE_H
