#ifndef DLGCREATEPROJECT_H
#define DLGCREATEPROJECT_H

#include <QDialog>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QDir>

#include "gversion.h"

namespace Ui {
class DlgCreateProject;
}

class DlgCreateProject : public QDialog
{
    Q_OBJECT
    
    public:
        explicit DlgCreateProject(QWidget *parent = 0);
        ~DlgCreateProject();

        void getCreateProjectAttrs(QString &name, QString &path, QString &password, bool &ok);

    private slots:
        void on_pbDirDlg_clicked();
        void on_pbCancel_clicked();
        void on_pbCreate_clicked();

    private:
        Ui::DlgCreateProject *ui;

        bool validateFields();
};

#endif // DLGCREATEPROJECT_H
