﻿#include "dlgcreateproject.h"
#include "ui_dlgcreateproject.h"

DlgCreateProject::DlgCreateProject(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgCreateProject)
{
    ui->setupUi(this);
}

DlgCreateProject::~DlgCreateProject()
{
    delete ui;
}

void DlgCreateProject::getCreateProjectAttrs(QString &name, QString &path,QString &password,bool &ok)
{
    DlgCreateProject dlgCreateProject;

    if (dlgCreateProject.exec()==Accepted){
        name.clear();
        path.clear();

        name=dlgCreateProject.ui->leName->text();
        path=dlgCreateProject.ui->lePath->text();
        password=dlgCreateProject.ui->lePassword->text();

        ok=true;
        return;
    }

    ok=false;
}

void DlgCreateProject::on_pbDirDlg_clicked()
{
    QString pathProject=QFileDialog::getExistingDirectory(this,"Папка");
    if (!pathProject.isEmpty())
        ui->lePath->setText(pathProject);
}

void DlgCreateProject::on_pbCancel_clicked()
{
    reject();
}

void DlgCreateProject::on_pbCreate_clicked()
{
    if (validateFields()){
        accept();
    }
}

bool DlgCreateProject::validateFields()
{
    if (ui->lePassword->text().isEmpty()){
        QMessageBox::warning(this,PROG_NAME,"Пароль не может быть пустым!");
        return false;
    }

    if (ui->lePassword->text()!=ui->leRepeatPassword->text()){
        QMessageBox::warning(this,PROG_NAME,"Пароли не совпадают!");
        return false;
    }

    if (ui->leName->text().trimmed().isEmpty()){
        QMessageBox::warning(this,PROG_NAME,"Имя проекта не может быть пустым!");
        return false;
    }

    ui->leName->setText(ui->leName->text().trimmed());

    if (ui->lePath->text().isEmpty()){
        QMessageBox::warning(this,PROG_NAME,"Путь не может быть пустым!");
        return false;
    }

    QDir dir(ui->lePath->text());
    if (!dir.exists()){
        int answer=QMessageBox::question(this,PROG_NAME,"Путь не существует! Создать",QMessageBox::Ok,QMessageBox::No);
        if (answer==QMessageBox::Ok){
            if (!dir.mkpath(ui->lePath->text())){
                QMessageBox::warning(this,PROG_NAME,"Не могу создать каталог!");
                return false;
            }
        }
    }

    return true;
}
