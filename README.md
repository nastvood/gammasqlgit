﻿# GammaSqlGit

## Сборка

Для сборки требуется

* [Qt4](http://qt-project.org/)
* [libgit2](http://libgit2.github.com/) 
* [botan](http://botan.randombit.net/)

## Бинарник

[GammaSqlGit.zip](https://drive.google.com/file/d/0Bz9GeIpo2mv_d0pNTndqYzNmZjA/edit?usp=sharing)

